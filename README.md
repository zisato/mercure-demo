Mercure Demo

WIP demo of api using ES + CQRS and Mercure for publishing events in hub 

## Requirements:

  * Docker


## SetUp:

Install composer dependencies:

`
./bin/composer.sh install
`

Start docker containers:

`
./bin/up.sh
`

Stop docker containers:

`
./bin/down.sh
`

## Run event bus message consumer

`
./bin/bash.sh php

php bin/console messenger:consume
`

## Execute tests:

`
./bin/test.sh [suite]
`

`
./bin/test-coverage.sh [suite]
`

## Execute console command

`
./bin/console.sh [command]
`


## Execute some tools
```
./bin/phpcbf.sh src/
./bin/phpstan.sh -l 7 src/
```