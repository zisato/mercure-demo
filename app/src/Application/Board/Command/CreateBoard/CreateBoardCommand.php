<?php

declare(strict_types=1);

namespace RetroBoard\Application\Board\Command\CreateBoard;

use RetroBoard\Domain\Board\WriteModel\ValueObject\BackgroundColor;
use RetroBoard\Domain\Board\WriteModel\ValueObject\Title;
use Zisato\CQRS\WriteModel\ValueObject\Command;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Identity\IdentityInterface;

class CreateBoardCommand implements Command
{
    private string $id;

    private string $title;

    private string $meetingId;

    private ?string $backgroundColor;

    public function __construct(string $id, string $title, string $meetingId, ?string $backgroundColor)
    {
        $this->id = $id;
        $this->title = $title;
        $this->meetingId = $meetingId;
        $this->backgroundColor = $backgroundColor;
    }

    public function id(): IdentityInterface
    {
        return UUID::fromString($this->id);
    }

    public function title(): Title
    {
        return Title::fromValue($this->title);
    }

    public function meetingId(): IdentityInterface
    {
        return UUID::fromString($this->meetingId);
    }

    public function backgroundColor(): ?BackgroundColor
    {
        return $this->backgroundColor
            ? BackgroundColor::fromValue($this->backgroundColor)
            : null;
    }
}
