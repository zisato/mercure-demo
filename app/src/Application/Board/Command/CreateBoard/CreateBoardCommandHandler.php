<?php

declare(strict_types=1);

namespace RetroBoard\Application\Board\Command\CreateBoard;

use RetroBoard\Domain\Board\WriteModel\Service\CreateBoardService;
use Zisato\CQRS\WriteModel\Service\CommandHandler;

class CreateBoardCommandHandler implements CommandHandler
{
    private CreateBoardService $createBoardService;

    public function __construct(CreateBoardService $createBoardService)
    {
        $this->createBoardService = $createBoardService;
    }

    public function __invoke(CreateBoardCommand $command): void
    {
        $this->createBoardService->create(
            $command->id(),
            $command->meetingId(),
            $command->title(),
            $command->backgroundColor()
        );
    }
}
