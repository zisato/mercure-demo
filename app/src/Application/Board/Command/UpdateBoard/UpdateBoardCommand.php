<?php

declare(strict_types=1);

namespace RetroBoard\Application\Board\Command\UpdateBoard;

use RetroBoard\Domain\Board\WriteModel\ValueObject\BackgroundColor;
use RetroBoard\Domain\Board\WriteModel\ValueObject\Title;
use Zisato\CQRS\WriteModel\ValueObject\Command;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Identity\IdentityInterface;

class UpdateBoardCommand implements Command
{
    private const INDEX_TITLE = 'attributes.name';

    private const INDEX_BACKGROUND_COLOR = 'attributes.background_color';

    private string $id;

    /**
     * @var array<string|int, mixed>
     */
    private array $propertiesToUpdate;

    private ?string $title;

    private ?string $backgroundColor;

    /**
     * @param array<string|int, mixed> $propertiesToUpdate
     */
    public function __construct(string $id, array $propertiesToUpdate, ?string $title, ?string $backgroundColor)
    {
        $this->id = $id;
        $this->propertiesToUpdate = $propertiesToUpdate;
        $this->title = $title;
        $this->backgroundColor = $backgroundColor;
    }

    public function id(): IdentityInterface
    {
        return UUID::fromString($this->id);
    }

    public function needsToUpdateTitle(): bool
    {
        return $this->needsToUpdate(self::INDEX_TITLE);
    }

    public function title(): ?Title
    {
        return Title::fromValue($this->title);
    }

    public function needsToUpdateBackgroundColor(): bool
    {
        return $this->needsToUpdate(self::INDEX_BACKGROUND_COLOR);
    }

    public function backgroundColor(): ?BackgroundColor
    {
        return $this->backgroundColor
            ? BackgroundColor::fromValue($this->backgroundColor)
            : null;
    }

    private function needsToUpdate(string $property): bool
    {
        return \in_array($property, $this->propertiesToUpdate, true);
    }
}
