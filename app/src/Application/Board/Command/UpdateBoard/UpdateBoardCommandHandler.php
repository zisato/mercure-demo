<?php

declare(strict_types=1);

namespace RetroBoard\Application\Board\Command\UpdateBoard;

use RetroBoard\Domain\Board\WriteModel\Repository\BoardRepository;
use Zisato\CQRS\WriteModel\Service\CommandHandler;

class UpdateBoardCommandHandler implements CommandHandler
{
    private BoardRepository $boardRepository;

    public function __construct(BoardRepository $boardRepository)
    {
        $this->boardRepository = $boardRepository;
    }

    public function __invoke(UpdateBoardCommand $command): void
    {
        $board = $this->boardRepository->get($command->id());

        if ($command->needsToUpdateTitle()) {
            $board->changeTitle($command->title());
        }

        if ($command->needsToUpdateBackgroundColor()) {
            $board->changeBackgroundColor($command->backgroundColor());
        }

        $this->boardRepository->save($board);
    }
}
