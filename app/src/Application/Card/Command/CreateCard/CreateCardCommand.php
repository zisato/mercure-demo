<?php

declare(strict_types=1);

namespace RetroBoard\Application\Card\Command\CreateCard;

use RetroBoard\Domain\Card\WriteModel\ValueObject\Text;
use Zisato\CQRS\WriteModel\ValueObject\Command;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Identity\IdentityInterface;

class CreateCardCommand implements Command
{
    private string $id;

    private string $text;

    private string $boardId;

    public function __construct(string $id, string $text, string $boardId)
    {
        $this->id = $id;
        $this->text = $text;
        $this->boardId = $boardId;
    }

    public function cardId(): IdentityInterface
    {
        return UUID::fromString($this->id);
    }

    public function text(): Text
    {
        return Text::fromValue($this->text);
    }

    public function boardId(): IdentityInterface
    {
        return UUID::fromString($this->boardId);
    }
}
