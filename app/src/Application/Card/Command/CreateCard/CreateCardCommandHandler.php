<?php

declare(strict_types=1);

namespace RetroBoard\Application\Card\Command\CreateCard;

use RetroBoard\Domain\Card\WriteModel\Service\CreateCardService;
use Zisato\CQRS\WriteModel\Service\CommandHandler;

class CreateCardCommandHandler implements CommandHandler
{
    private CreateCardService $createCardService;

    public function __construct(
        CreateCardService $createCardService
    ) {
        $this->createCardService = $createCardService;
    }

    public function __invoke(CreateCardCommand $command): void
    {
        $this->createCardService->create($command->cardId(), $command->boardId(), $command->text());
    }
}
