<?php

declare(strict_types=1);

namespace RetroBoard\Application\Card\Command\DeleteCard;

use Zisato\CQRS\WriteModel\ValueObject\Command;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Identity\IdentityInterface;

class DeleteCardCommand implements Command
{
    private string $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function id(): IdentityInterface
    {
        return UUID::fromString($this->id);
    }
}
