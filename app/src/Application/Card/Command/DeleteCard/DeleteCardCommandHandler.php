<?php

declare(strict_types=1);

namespace RetroBoard\Application\Card\Command\DeleteCard;

use RetroBoard\Domain\Card\WriteModel\Repository\CardRepository;
use Zisato\CQRS\WriteModel\Service\CommandHandler;

class DeleteCardCommandHandler implements CommandHandler
{
    private CardRepository $cardRepository;

    public function __construct(CardRepository $cardRepository)
    {
        $this->cardRepository = $cardRepository;
    }

    public function __invoke(DeleteCardCommand $command): void
    {
        $card = $this->cardRepository->get($command->id());

        $card->delete();

        $this->cardRepository->save($card);
    }
}
