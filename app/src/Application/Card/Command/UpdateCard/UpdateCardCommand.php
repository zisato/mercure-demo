<?php

declare(strict_types=1);

namespace RetroBoard\Application\Card\Command\UpdateCard;

use RetroBoard\Domain\Card\WriteModel\ValueObject\Text;
use Zisato\CQRS\WriteModel\ValueObject\Command;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Identity\IdentityInterface;

class UpdateCardCommand implements Command
{
    private const INDEX_TEXT = 'attributes.text';

    private string $id;

    /**
     * @var array<string|int, mixed>
     */
    private array $propertiesToUpdate;

    private ?string $text;

    /**
     * @param array<string|int, mixed> $propertiesToUpdate
     */
    public function __construct(string $id, array $propertiesToUpdate, ?string $text)
    {
        $this->id = $id;
        $this->propertiesToUpdate = $propertiesToUpdate;
        $this->text = $text;
    }

    public function id(): IdentityInterface
    {
        return UUID::fromString($this->id);
    }

    public function needsToUpdateText(): bool
    {
        return $this->needsToUpdate(self::INDEX_TEXT);
    }

    public function text(): ?Text
    {
        return $this->text ? Text::fromValue($this->text) : null;
    }

    private function needsToUpdate(string $property): bool
    {
        return \in_array($property, $this->propertiesToUpdate, true);
    }
}
