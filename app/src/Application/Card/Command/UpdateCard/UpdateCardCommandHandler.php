<?php

declare(strict_types=1);

namespace RetroBoard\Application\Card\Command\UpdateCard;

use RetroBoard\Domain\Card\WriteModel\Repository\CardRepository;
use Zisato\CQRS\WriteModel\Service\CommandHandler;

class UpdateCardCommandHandler implements CommandHandler
{
    private CardRepository $cardRepository;

    public function __construct(CardRepository $cardRepository)
    {
        $this->cardRepository = $cardRepository;
    }

    public function __invoke(UpdateCardCommand $command): void
    {
        $card = $this->cardRepository->get($command->id());

        if ($command->needsToUpdateText()) {
            $card->changeText($command->text());
        }

        $this->cardRepository->save($card);
    }
}
