<?php

declare(strict_types=1);

namespace RetroBoard\Application\Meeting\Command\CreateMeeting;

use RetroBoard\Domain\Meeting\WriteModel\ValueObject\Name;
use Zisato\CQRS\WriteModel\ValueObject\Command;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Identity\IdentityInterface;

class CreateMeetingCommand implements Command
{
    private string $id;

    private string $name;

    public function __construct(string $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public function id(): IdentityInterface
    {
        return UUID::fromString($this->id);
    }

    public function name(): Name
    {
        return Name::fromValue($this->name);
    }
}
