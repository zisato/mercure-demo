<?php

declare(strict_types=1);

namespace RetroBoard\Application\Meeting\Command\CreateMeeting;

use RetroBoard\Domain\Meeting\WriteModel\Service\CreateMeetingService;
use Zisato\CQRS\WriteModel\Service\CommandHandler;

class CreateMeetingCommandHandler implements CommandHandler
{
    private CreateMeetingService $createMeetingService;

    public function __construct(CreateMeetingService $createMeetingService)
    {
        $this->createMeetingService = $createMeetingService;
    }

    public function __invoke(CreateMeetingCommand $command): void
    {
        $this->createMeetingService->create($command->id(), $command->name());
    }
}
