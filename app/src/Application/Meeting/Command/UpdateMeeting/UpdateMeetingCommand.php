<?php

declare(strict_types=1);

namespace RetroBoard\Application\Meeting\Command\UpdateMeeting;

use RetroBoard\Domain\Meeting\WriteModel\ValueObject\Name;
use Zisato\CQRS\WriteModel\ValueObject\Command;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Identity\IdentityInterface;

class UpdateMeetingCommand implements Command
{
    private const INDEX_NAME = 'attributes.name';

    private string $id;

    /**
     * @var array<string|int, mixed>
     */
    private array $propertiesToUpdate;

    private ?string $name;

    /**
     * @param array<string|int, mixed> $propertiesToUpdate
     */
    public function __construct(string $id, array $propertiesToUpdate, ?string $name)
    {
        $this->id = $id;
        $this->propertiesToUpdate = $propertiesToUpdate;
        $this->name = $name;
    }

    public function id(): IdentityInterface
    {
        return UUID::fromString($this->id);
    }

    public function needsToUpdateName(): bool
    {
        return $this->needsToUpdate(self::INDEX_NAME);
    }

    public function name(): ?Name
    {
        return Name::fromValue($this->name);
    }

    private function needsToUpdate(string $property): bool
    {
        return \in_array($property, $this->propertiesToUpdate, true);
    }
}
