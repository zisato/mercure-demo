<?php

declare(strict_types=1);

namespace RetroBoard\Application\Meeting\Command\UpdateMeeting;

use RetroBoard\Domain\Meeting\WriteModel\Repository\MeetingRepository;
use Zisato\CQRS\WriteModel\Service\CommandHandler;

class UpdateMeetingCommandHandler implements CommandHandler
{
    private MeetingRepository $MeetingRepository;

    public function __construct(MeetingRepository $MeetingRepository)
    {
        $this->MeetingRepository = $MeetingRepository;
    }

    public function __invoke(UpdateMeetingCommand $command): void
    {
        $meeting = $this->MeetingRepository->get($command->id());

        if ($command->needsToUpdateName()) {
            $meeting->changeName($command->name());
        }

        $this->MeetingRepository->save($meeting);
    }
}
