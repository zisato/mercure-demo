<?php

declare(strict_types=1);

namespace RetroBoard\Application\Meeting\Query\DetailMeeting;

use Zisato\CQRS\ReadModel\ValueObject\Query;

class DetailMeetingQuery implements Query
{
    private string $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function id(): string
    {
        return $this->id;
    }
}
