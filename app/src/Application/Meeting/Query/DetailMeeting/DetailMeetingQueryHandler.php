<?php

declare(strict_types=1);

namespace RetroBoard\Application\Meeting\Query\DetailMeeting;

use RetroBoard\Application\Meeting\Transformer\MeetingTransformer;
use RetroBoard\Domain\Meeting\ReadModel\Repository\MeetingsProjectionReadRepository;
use Zisato\ApiBundle\Application\ReadModel\ValueObject\ProjectionQueryResult;
use Zisato\CQRS\ReadModel\Service\QueryHandler;
use Zisato\CQRS\ReadModel\ValueObject\QueryResult;

class DetailMeetingQueryHandler implements QueryHandler
{
    private MeetingsProjectionReadRepository $meetingsProjectionReadRepository;

    private MeetingTransformer $meetingTransformer;

    public function __construct(
        MeetingsProjectionReadRepository $meetingsProjectionReadRepository,
        MeetingTransformer $meetingTransformer
    ) {
        $this->meetingsProjectionReadRepository = $meetingsProjectionReadRepository;
        $this->meetingTransformer = $meetingTransformer;
    }

    public function __invoke(DetailMeetingQuery $query): QueryResult
    {
        $meeting = $this->meetingsProjectionReadRepository->get($query->id());

        return ProjectionQueryResult::create($meeting, $this->meetingTransformer);
    }
}
