<?php

declare(strict_types=1);

namespace RetroBoard\Application\Meeting\Query\ListMeetings;

use Zisato\ApiBundle\Application\ReadModel\ValueObject\Filters\Filter;
use Zisato\ApiBundle\Application\ReadModel\ValueObject\Filters\FilterCollection;
use Zisato\ApiBundle\Application\ReadModel\ValueObject\Filters\Type;
use Zisato\ApiBundle\Application\ReadModel\ValueObject\ListableProjectionQuery;
use Zisato\ApiBundle\Application\ReadModel\ValueObject\SortBy\SortBy;
use Zisato\ApiBundle\Application\ReadModel\ValueObject\SortBy\SortByCollection;

class ListMeetingsQuery extends ListableProjectionQuery
{
    public function offset(): int
    {
        return ($this->page() - 1) * $this->perPage();
    }

    protected function allowedFilters(): FilterCollection
    {
        return new FilterCollection(new Filter('attributes.name', Type::string()));
    }

    protected function allowedSortBy(): SortByCollection
    {
        return new SortByCollection(new SortBy('attributes.name'));
    }
}
