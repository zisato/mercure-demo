<?php

declare(strict_types=1);

namespace RetroBoard\Application\Meeting\Query\ListMeetings;

use RetroBoard\Application\Meeting\Transformer\MeetingTransformer;
use RetroBoard\Domain\Meeting\ReadModel\Repository\MeetingsProjectionReadRepository;
use Zisato\ApiBundle\Application\ReadModel\ValueObject\ListableProjectionQueryResult;
use Zisato\CQRS\ReadModel\Service\QueryHandler;
use Zisato\CQRS\ReadModel\ValueObject\ListableQueryResult;

class ListMeetingsQueryHandler implements QueryHandler
{
    private MeetingsProjectionReadRepository $meetingsProjectionReadRepository;

    private MeetingTransformer $meetingTransformer;

    public function __construct(
        MeetingsProjectionReadRepository $meetingsProjectionReadRepository,
        MeetingTransformer $meetingTransformer
    ) {
        $this->meetingsProjectionReadRepository = $meetingsProjectionReadRepository;
        $this->meetingTransformer = $meetingTransformer;
    }

    public function __invoke(ListMeetingsQuery $query): ListableQueryResult
    {
        $meetingsCollection = $this->meetingsProjectionReadRepository->findBy(
            $query->criteria(),
            $query->offset(),
            $query->perPage(),
            $query->orderBy(),
        );

        return ListableProjectionQueryResult::create(
            $meetingsCollection,
            $this->meetingTransformer,
            $meetingsCollection->total(),
            $query->page(),
            $query->perPage()
        );
    }
}
