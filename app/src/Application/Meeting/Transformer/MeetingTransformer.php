<?php

declare(strict_types=1);

namespace RetroBoard\Application\Meeting\Transformer;

use RetroBoard\Domain\Meeting\ReadModel\ValueObject\MeetingProjectionModel;
use Zisato\Projection\Transformer\ProjectionModelTransformer;
use Zisato\Projection\ValueObject\ProjectionModel;

class MeetingTransformer implements ProjectionModelTransformer
{
    /**
     * @param MeetingProjectionModel $projectionModel
     *
     * @return array<string, mixed>
     */
    public function transform(ProjectionModel $projectionModel): array
    {
        return [
            'id' => $projectionModel->id(),
            'attributes' => [
                'name' => $projectionModel->name(),
            ],
            'relationships' => [
                'boards' => $projectionModel->boards(),
            ],
        ];
    }
}
