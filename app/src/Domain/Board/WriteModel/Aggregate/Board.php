<?php

declare(strict_types=1);

namespace RetroBoard\Domain\Board\WriteModel\Aggregate;

use RetroBoard\Domain\Board\WriteModel\Event\BoardBackgroundColorChanged;
use RetroBoard\Domain\Board\WriteModel\Event\BoardCreated;
use RetroBoard\Domain\Board\WriteModel\Event\BoardTitleChanged;
use RetroBoard\Domain\Board\WriteModel\ValueObject\BackgroundColor;
use RetroBoard\Domain\Board\WriteModel\ValueObject\Title;
use Zisato\EventSourcing\Aggregate\AbstractAggregateRoot;
use Zisato\EventSourcing\Identity\IdentityInterface;

class Board extends AbstractAggregateRoot
{
    private Title $title;

    private IdentityInterface $meetingId;

    private ?BackgroundColor $backgroundColor;

    public static function create(
        IdentityInterface $aggregateId,
        Title $title,
        IdentityInterface $meetingId,
        ?BackgroundColor $backgroundColor
    ): self {
        $instance = new self($aggregateId);

        $instance->recordThat(BoardCreated::create($aggregateId, $title, $meetingId, $backgroundColor));

        return $instance;
    }

    public function meetingId(): IdentityInterface
    {
        return clone $this->meetingId;
    }

    public function changeTitle(Title $newTitle): void
    {
        if ($this->title->equals($newTitle) === false) {
            $this->recordThat(BoardTitleChanged::create($this->id(), $this->title, $newTitle));
        }
    }

    public function changeBackgroundColor(?BackgroundColor $newBackgroundColor): void
    {
        $previousBackgroundColor = $this->backgroundColor ? $this->backgroundColor->value() : null;
        $newBackgroundColorValue = $newBackgroundColor ? $newBackgroundColor->value() : null;

        if ($previousBackgroundColor !== $newBackgroundColorValue) {
            $this->recordThat(
                BoardBackgroundColorChanged::create($this->id(), $this->backgroundColor, $newBackgroundColor)
            );
        }
    }

    public function applyBoardTitleChanged(BoardTitleChanged $event): void
    {
        $this->title = $event->title();
    }

    public function applyBoardBackgroundColorChanged(BoardBackgroundColorChanged $event): void
    {
        $this->backgroundColor = $event->backgroundColor();
    }

    protected function applyBoardCreated(BoardCreated $event): void
    {
        $this->title = $event->title();
        $this->meetingId = $event->meetingId();
        $this->backgroundColor = $event->backgroundColor();
    }
}
