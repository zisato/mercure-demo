<?php

declare(strict_types=1);

namespace RetroBoard\Domain\Board\WriteModel\Event;

use RetroBoard\Domain\Board\WriteModel\ValueObject\BackgroundColor;
use Zisato\EventSourcing\Aggregate\Event\AbstractEvent;
use Zisato\EventSourcing\Identity\IdentityInterface;

class BoardBackgroundColorChanged extends AbstractEvent
{
    private const DEFAULT_VERSION = 1;

    private const INDEX_NEW_VALUE = 'new_background_color';

    private const INDEX_PREVIOUS_VALUE = 'previous_background_color';

    public static function defaultVersion(): int
    {
        return static::DEFAULT_VERSION;
    }

    public static function create(
        IdentityInterface $aggregateId,
        ?BackgroundColor $previousBackgroundColor,
        ?BackgroundColor $newBackgroundColor
    ): self {
        /** @var BoardBackgroundColorChanged $event */
        $event = self::occur(
            $aggregateId->value(),
            [
                self::INDEX_PREVIOUS_VALUE => $previousBackgroundColor
                    ? $previousBackgroundColor->value()
                    : null,
                self::INDEX_NEW_VALUE => $newBackgroundColor
                    ? $newBackgroundColor->value()
                    : null,
            ]
        );

        return $event;
    }

    public function backgroundColor(): ?BackgroundColor
    {
        return $this->payload()[self::INDEX_NEW_VALUE]
            ? BackgroundColor::fromValue($this->payload()[self::INDEX_NEW_VALUE])
            : null;
    }
}
