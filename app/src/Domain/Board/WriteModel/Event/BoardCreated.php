<?php

declare(strict_types=1);

namespace RetroBoard\Domain\Board\WriteModel\Event;

use RetroBoard\Domain\Board\WriteModel\ValueObject\BackgroundColor;
use RetroBoard\Domain\Board\WriteModel\ValueObject\Title;
use Zisato\EventSourcing\Aggregate\Event\AbstractEvent;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Identity\IdentityInterface;

class BoardCreated extends AbstractEvent
{
    private const DEFAULT_VERSION = 2;

    private const INDEX_TITLE = 'title';

    private const INDEX_MEETING_ID = 'meeting_id';

    private const INDEX_BACKGROUND_COLOR = 'background_color';

    public static function defaultVersion(): int
    {
        return static::DEFAULT_VERSION;
    }

    public static function create(
        IdentityInterface $aggregateId,
        Title $title,
        IdentityInterface $meetingId,
        ?BackgroundColor $backgroundColor
    ): self {
        /** @var BoardCreated $event */
        $event = self::occur(
            $aggregateId->value(),
            [
                self::INDEX_TITLE => $title->value(),
                self::INDEX_MEETING_ID => $meetingId->value(),
                self::INDEX_BACKGROUND_COLOR => $backgroundColor
                    ? $backgroundColor->value()
                    : null,
            ]
        );

        return $event;
    }

    public function title(): Title
    {
        return Title::fromValue($this->payload()[self::INDEX_TITLE]);
    }

    public function meetingId(): IdentityInterface
    {
        return UUID::fromString($this->payload()[self::INDEX_MEETING_ID]);
    }

    public function backgroundColor(): ?BackgroundColor
    {
        return $this->payload()[self::INDEX_BACKGROUND_COLOR]
            ? BackgroundColor::fromValue($this->payload()[self::INDEX_BACKGROUND_COLOR])
            : null;
    }
}
