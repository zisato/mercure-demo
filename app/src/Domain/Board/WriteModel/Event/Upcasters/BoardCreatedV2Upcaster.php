<?php

declare(strict_types=1);

namespace RetroBoard\Domain\Board\WriteModel\Event\Upcasters;

use RetroBoard\Domain\Board\WriteModel\Event\BoardCreated;
use Zisato\EventSourcing\Aggregate\Event\EventInterface;
use Zisato\EventSourcing\Aggregate\Event\Upcast\UpcasterInterface;

class BoardCreatedV2Upcaster implements UpcasterInterface
{
    private const INDEX_BACKGROUND_COLOR = 'background_color';

    private const VERSION_FROM = 1;

    private const VERSION_TO = 2;

    public function canUpcast(EventInterface $event): bool
    {
        return $event->version() === self::VERSION_FROM;
    }

    public function upcast(EventInterface $event): BoardCreated
    {
        $newPayload = $event->payload();
        $newPayload[self::INDEX_BACKGROUND_COLOR] = null;

        return BoardCreated::reconstitute(
            $event->aggregateId(),
            $event->aggregateVersion(),
            $event->createdAt(),
            $newPayload,
            self::VERSION_TO,
            $event->metadata()
        );
    }
}
