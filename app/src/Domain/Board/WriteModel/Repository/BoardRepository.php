<?php

declare(strict_types=1);

namespace RetroBoard\Domain\Board\WriteModel\Repository;

use RetroBoard\Domain\Board\WriteModel\Aggregate\Board;
use Zisato\EventSourcing\Aggregate\Repository\AggregateRootRepositoryInterface;
use Zisato\EventSourcing\Identity\IdentityInterface;

interface BoardRepository extends AggregateRootRepositoryInterface
{
    public function get(IdentityInterface $aggregateId): Board;
}
