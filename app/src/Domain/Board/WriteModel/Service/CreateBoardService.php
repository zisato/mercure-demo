<?php

declare(strict_types=1);

namespace RetroBoard\Domain\Board\WriteModel\Service;

use RetroBoard\Domain\Board\WriteModel\Aggregate\Board;
use RetroBoard\Domain\Board\WriteModel\Repository\BoardRepository;
use RetroBoard\Domain\Board\WriteModel\ValueObject\BackgroundColor;
use RetroBoard\Domain\Board\WriteModel\ValueObject\Title;
use RetroBoard\Domain\Meeting\WriteModel\Repository\MeetingRepository;
use Zisato\EventSourcing\Aggregate\Exception\AggregateRootNotFoundException;
use Zisato\EventSourcing\Aggregate\Exception\DuplicatedAggregateIdException;
use Zisato\EventSourcing\Identity\IdentityInterface;

class CreateBoardService
{
    private MeetingRepository $meetingRepository;

    private BoardRepository $boardRepository;

    public function __construct(MeetingRepository $meetingRepository, BoardRepository $boardRepository)
    {
        $this->meetingRepository = $meetingRepository;
        $this->boardRepository = $boardRepository;
    }

    public function create(
        IdentityInterface $boardId,
        IdentityInterface $meetingId,
        Title $title,
        ?BackgroundColor $backgroundColor
    ): void {
        $this->assertDuplicatedBoardId($boardId);
        
        $meeting = $this->meetingRepository->get($meetingId);

        $board = Board::create($boardId, $title, $meeting->id(), $backgroundColor);

        $this->boardRepository->save($board);
    }

    private function assertDuplicatedBoardId(IdentityInterface $id): void
    {
        try {
            $this->boardRepository->get($id);

            throw new DuplicatedAggregateIdException(\sprintf(
                'Aggregated id %s exists in repository.',
                $id->value()
            ));
        } catch (AggregateRootNotFoundException $exception) {}
    }
}
