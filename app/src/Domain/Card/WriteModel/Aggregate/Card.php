<?php

declare(strict_types=1);

namespace RetroBoard\Domain\Card\WriteModel\Aggregate;

use RetroBoard\Domain\Card\WriteModel\Event\CardCreated;
use RetroBoard\Domain\Card\WriteModel\Event\CardDeleted;
use RetroBoard\Domain\Card\WriteModel\Event\CardTextChanged;
use RetroBoard\Domain\Card\WriteModel\ValueObject\Text;
use Zisato\EventSourcing\Aggregate\AbstractAggregateRoot;
use Zisato\EventSourcing\Identity\IdentityInterface;

class Card extends AbstractAggregateRoot
{
    private Text $text;

    private IdentityInterface $boardId;

    private bool $deleted;

    public static function create(
        IdentityInterface $aggregateId,
        Text $text,
        IdentityInterface $boardId
    ): self {
        $instance = new self($aggregateId);

        $instance->recordThat(CardCreated::create($aggregateId, $text, $boardId));

        return $instance;
    }

    public function text(): Text
    {
        return $this->text;
    }

    public function boardId(): IdentityInterface
    {
        return $this->boardId;
    }

    public function changeText(Text $newText): void
    {
        if ($this->text->equals($newText) === false) {
            $this->recordThat(CardTextChanged::create($this->id(), $this->boardId(), $this->text, $newText));
        }
    }

    public function delete(): void
    {
        if ($this->deleted === false) {
            $this->recordThat(CardDeleted::create($this->id(), $this->boardId()));
        }
    }

    public function applyCardCreated(CardCreated $event): void
    {
        $this->text = $event->text();
        $this->boardId = $event->boardId();
        $this->deleted = false;
    }

    public function applyCardTextChanged(CardTextChanged $event): void
    {
        $this->text = $event->text();
    }

    public function applyCardDeleted(CardDeleted $event): void
    {
        $this->deleted = $event->deleted();
    }
}
