<?php

declare(strict_types=1);

namespace RetroBoard\Domain\Card\WriteModel\Event;

use RetroBoard\Domain\Card\WriteModel\ValueObject\Text;
use Zisato\EventSourcing\Aggregate\Event\AbstractEvent;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Identity\IdentityInterface;

class CardCreated extends AbstractEvent
{
    private const DEFAULT_VERSION = 1;

    private const INDEX_TEXT = 'text';

    private const INDEX_BOARD_ID = 'board_id';

    public static function defaultVersion(): int
    {
        return static::DEFAULT_VERSION;
    }

    public static function create(
        IdentityInterface $aggregateId,
        Text $text,
        IdentityInterface $boardId
    ): self {
        /** @var CardCreated $event */
        $event = self::occur(
            $aggregateId->value(),
            [
                self::INDEX_TEXT => $text->value(),
                self::INDEX_BOARD_ID => $boardId->value(),
            ]
        );

        return $event;
    }

    public function text(): Text
    {
        return Text::fromValue($this->payload()[self::INDEX_TEXT]);
    }

    public function boardId(): IdentityInterface
    {
        return UUID::fromString($this->payload()[self::INDEX_BOARD_ID]);
    }
}
