<?php

declare(strict_types=1);

namespace RetroBoard\Domain\Card\WriteModel\Event;

use Zisato\EventSourcing\Aggregate\Event\AbstractEvent;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Identity\IdentityInterface;

class CardDeleted extends AbstractEvent
{
    private const DEFAULT_VERSION = 1;

    private const INDEX_VALUE = 'deleted';

    private const INDEX_BOARD_ID = 'board_id';

    public static function defaultVersion(): int
    {
        return static::DEFAULT_VERSION;
    }

    public static function create(IdentityInterface $aggregateId, IdentityInterface $boardId): self
    {
        /** @var CardDeleted $event */
        $event = self::occur($aggregateId->value(), [
            self::INDEX_VALUE => true,
            self::INDEX_BOARD_ID => $boardId->value(),
        ]);

        return $event;
    }

    public function deleted(): bool
    {
        return $this->payload()[self::INDEX_VALUE];
    }

    public function boardId(): IdentityInterface
    {
        return UUID::fromString($this->payload()[self::INDEX_BOARD_ID]);
    }
}
