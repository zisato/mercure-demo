<?php

declare(strict_types=1);

namespace RetroBoard\Domain\Card\WriteModel\Event;

use RetroBoard\Domain\Card\WriteModel\ValueObject\Text;
use Zisato\EventSourcing\Aggregate\Event\AbstractEvent;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Identity\IdentityInterface;

class CardTextChanged extends AbstractEvent
{
    private const DEFAULT_VERSION = 1;

    private const INDEX_NEW_VALUE = 'new_text';

    private const INDEX_PREVIOUS_VALUE = 'previous_text';

    private const INDEX_BOARD_ID = 'board_id';

    public static function defaultVersion(): int
    {
        return static::DEFAULT_VERSION;
    }

    public static function create(IdentityInterface $aggregateId, IdentityInterface $boardId, Text $previousText, Text $newText): self
    {
        /** @var CardTextChanged $event */
        $event = self::occur(
            $aggregateId->value(),
            [
                self::INDEX_PREVIOUS_VALUE => $previousText->value(),
                self::INDEX_NEW_VALUE => $newText->value(),
                self::INDEX_BOARD_ID => $boardId->value(),
            ]
        );

        return $event;
    }

    public function text(): Text
    {
        return Text::fromValue($this->payload()[self::INDEX_NEW_VALUE]);
    }

    public function boardId(): IdentityInterface
    {
        return UUID::fromString($this->payload()[self::INDEX_BOARD_ID]);
    }
}
