<?php

declare(strict_types=1);

namespace RetroBoard\Domain\Card\WriteModel\Repository;

use RetroBoard\Domain\Card\WriteModel\Aggregate\Card;
use Zisato\EventSourcing\Aggregate\Repository\AggregateRootRepositoryInterface;
use Zisato\EventSourcing\Identity\IdentityInterface;

interface CardRepository extends AggregateRootRepositoryInterface
{
    public function get(IdentityInterface $aggregateId): Card;
}
