<?php

declare(strict_types=1);

namespace RetroBoard\Domain\Card\WriteModel\Service;

use RetroBoard\Domain\Board\WriteModel\Repository\BoardRepository;
use RetroBoard\Domain\Card\WriteModel\Aggregate\Card;
use RetroBoard\Domain\Card\WriteModel\Repository\CardRepository;
use RetroBoard\Domain\Card\WriteModel\ValueObject\Text;
use Zisato\EventSourcing\Aggregate\Exception\AggregateRootNotFoundException;
use Zisato\EventSourcing\Aggregate\Exception\DuplicatedAggregateIdException;
use Zisato\EventSourcing\Identity\IdentityInterface;

class CreateCardService
{
    private CardRepository $cardRepository;

    private BoardRepository $boardRepository;

    public function __construct(
        CardRepository $cardRepository,
        BoardRepository $boardRepository
    ) {
        $this->cardRepository = $cardRepository;
        $this->boardRepository = $boardRepository;
    }

    public function create(
        IdentityInterface $cardId,
        IdentityInterface $boardId,
        Text $text
    ): void {
        $this->assertDuplicatedCardId($cardId);
        $this->assertBoardIdNotExists($boardId);

        $card = Card::create($cardId, $text, $boardId);

        $this->cardRepository->save($card);
    }

    public function assertDuplicatedCardId(IdentityInterface $id): void
    {
        try {
            $this->cardRepository->get($id);

            throw new DuplicatedAggregateIdException(\sprintf(
                'Aggregated id %s exists in repository.',
                $id->value()
            ));
        } catch (AggregateRootNotFoundException $exception) {
        }
    }

    private function assertBoardIdNotExists(IdentityInterface $id): void
    {
        $this->boardRepository->get($id);
    }
}
