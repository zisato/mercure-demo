<?php

declare(strict_types=1);

namespace RetroBoard\Domain\Meeting\ReadModel\Repository;

use RetroBoard\Domain\Meeting\ReadModel\ValueObject\MeetingProjectionModel;
use Zisato\Projection\Repository\ProjectionReadRepository;

interface MeetingsProjectionReadRepository extends ProjectionReadRepository
{
    public function get(string $id): MeetingProjectionModel;
}
