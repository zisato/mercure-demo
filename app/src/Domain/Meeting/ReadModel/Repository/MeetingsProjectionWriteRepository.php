<?php

declare(strict_types=1);

namespace RetroBoard\Domain\Meeting\ReadModel\Repository;

use RetroBoard\Domain\Meeting\ReadModel\ValueObject\MeetingProjectionModel;
use Zisato\Projection\Repository\ProjectionWriteRepository;
use Zisato\Projection\ValueObject\ProjectionModel;

interface MeetingsProjectionWriteRepository extends ProjectionWriteRepository
{
    /**
     * @param MeetingProjectionModel|ProjectionModel $projectionModel
     */
    public function save(ProjectionModel $projectionModel): void;
}
