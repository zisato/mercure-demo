<?php

declare(strict_types=1);

namespace RetroBoard\Domain\Meeting\ReadModel\ValueObject;

use RetroBoard\Domain\Board\WriteModel\ValueObject\BackgroundColor;
use RetroBoard\Domain\Board\WriteModel\ValueObject\Title;
use RetroBoard\Domain\Card\WriteModel\ValueObject\Text;
use Zisato\ApiBundle\Util\ArrayKeyFinder;
use Zisato\EventSourcing\Identity\IdentityInterface;
use Zisato\Projection\ValueObject\ProjectionModel;

class BoardProjectionModel extends ProjectionModel
{
    use ArrayKeyFinder;

    public static function fromData(array $data): self
    {
        return parent::fromData($data);
    }

    public static function create(
        IdentityInterface $id,
        Title $title,
        ?BackgroundColor $backgroundColor,
        IdentityInterface $meetingId
    ): self {
        /** @var BoardProjectionModel $result */
        $result = static::fromData([
            'id' => $id->value(),
            'attributes' => [
                'name' => $title->value(),
                'background_color' => $backgroundColor
                    ? $backgroundColor->value()
                    : null,
            ],
            'relationships' => [
                'meeting' => [
                    'id' => $meetingId->value(),
                ],
                'cards' => [],
            ],
        ]);

        return $result;
    }

    public function changeTitle(Title $title): void
    {
        $newData = $this->data();

        $newData['attributes']['name'] = $title->value();

        $this->changeData($newData);
    }

    public function changeBackgroundColor(?BackgroundColor $backgroundColor): void
    {
        $newData = $this->data();

        $newData['attributes']['background_color'] = $backgroundColor ? $backgroundColor->value() : null;

        $this->changeData($newData);
    }

    public function addCard(CardProjectionModel $card): void
    {
        $newData = $this->data();

        $newData['relationships']['cards'][] = $card->data();

        $this->changeData($newData);
    }

    public function removeCard(IdentityInterface $cardId): void
    {
        $cardIdValue = $cardId->value();
        $cards = $this->data()['relationships']['cards'];
        $cardIndex = $this->index(
            $cards,
            function (array $data) use ($cardIdValue) {
                return $data['id'] === $cardIdValue;
            }
        );

        unset($cards[$cardIndex]);

        $newData = $this->data();

        $newData['relationships']['cards'] = \array_values($cards);

        $this->changeData($newData);
    }

    public function changeCardText(IdentityInterface $cardId, Text $text): void
    {
        $cardIndex = $this->getCardIndexById($cardId);
        $card = CardProjectionModel::fromData($this->data()['relationships']['cards'][$cardIndex]);

        $card->changeText($text);

        $newData = $this->data();
        $newData['relationships']['cards'][$cardIndex] = $card->data();

        $this->changeData($newData);
    }

    private function getCardIndexById(IdentityInterface $cardId): int
    {
        $cardIdValue = $cardId->value();
        return $this->index(
            $this->data()['relationships']['cards'],
            function (array $data) use ($cardIdValue) {
                return $data['id'] === $cardIdValue;
            }
        );
    }
}
