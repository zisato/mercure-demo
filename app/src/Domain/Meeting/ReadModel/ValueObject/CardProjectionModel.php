<?php

declare(strict_types=1);

namespace RetroBoard\Domain\Meeting\ReadModel\ValueObject;

use RetroBoard\Domain\Card\WriteModel\ValueObject\Text;
use Zisato\EventSourcing\Identity\IdentityInterface;
use Zisato\Projection\ValueObject\ProjectionModel;

class CardProjectionModel extends ProjectionModel
{
    public static function fromData(array $data): self
    {
        return parent::fromData($data);
    }

    public static function create(
        IdentityInterface $id,
        Text $text,
        IdentityInterface $boardId
    ): self {
        /** @var CardProjectionModel $result */
        $result = static::fromData([
            'id' => $id->value(),
            'attributes' => [
                'text' => $text->value(),
            ],
            'relationships' => [
                'board' => [
                    'id' => $boardId->value(),
                ],
            ],
        ]);

        return $result;
    }

    public function changeText(Text $text): void
    {
        $newData = $this->data();

        $newData['attributes']['text'] = $text->value();

        $this->changeData($newData);
    }
}
