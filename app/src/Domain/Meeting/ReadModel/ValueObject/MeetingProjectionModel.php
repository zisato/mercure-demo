<?php

declare(strict_types=1);

namespace RetroBoard\Domain\Meeting\ReadModel\ValueObject;

use RetroBoard\Domain\Board\WriteModel\ValueObject\BackgroundColor;
use RetroBoard\Domain\Board\WriteModel\ValueObject\Title;
use RetroBoard\Domain\Card\WriteModel\ValueObject\Text;
use RetroBoard\Domain\Meeting\WriteModel\ValueObject\Name;
use Zisato\ApiBundle\Util\ArrayKeyFinder;
use Zisato\EventSourcing\Identity\IdentityInterface;
use Zisato\Projection\ValueObject\ProjectionModel;

class MeetingProjectionModel extends ProjectionModel
{
    use ArrayKeyFinder;

    public static function create(IdentityInterface $id, Name $name): self
    {
        /** @var MeetingProjectionModel $result */
        $result = static::fromData([
            'id' => $id->value(),
            'attributes' => [
                'name' => $name->value(),
            ],
            'relationships' => [
                'boards' => [],
            ],
        ]);

        return $result;
    }

    public function id(): string
    {
        return $this->data()['id'];
    }

    public function name(): string
    {
        return $this->data()['attributes']['name'];
    }

    /**
     * @return array<string, mixed>
     */
    public function boards(): array
    {
        return $this->data()['relationships']['boards'];
    }

    public function changeName(Name $name): void
    {
        $newData = $this->data();

        $newData['attributes']['name'] = $name->value();

        $this->changeData($newData);
    }

    public function addBoard(BoardProjectionModel $boardProjectionModel): void
    {
        $newData = $this->data();
        $boardData = $boardProjectionModel->data();

        unset($boardData['_id']);

        \array_push($newData['relationships']['boards'], $boardData);

        $this->changeData($newData);
    }

    public function changeBoardTitle(IdentityInterface $boardId, Title $title): void
    {
        $boardIndex = $this->getBoardIndexById($boardId);
        $board = BoardProjectionModel::fromData($this->data()['relationships']['boards'][$boardIndex]);

        $board->changeTitle($title);

        $newData = $this->data();
        $newData['relationships']['boards'][$boardIndex] = $board->data();

        $this->changeData($newData);
    }

    public function changeBoardBackgroundColor(IdentityInterface $boardId, ?BackgroundColor $backgroundColor): void
    {
        $boardIndex = $this->getBoardIndexById($boardId);
        $board = BoardProjectionModel::fromData($this->data()['relationships']['boards'][$boardIndex]);

        $board->changeBackgroundColor($backgroundColor);

        $newData = $this->data();
        $newData['relationships']['boards'][$boardIndex] = $board->data();

        $this->changeData($newData);
    }

    public function addCardToBoard(IdentityInterface $boardId, CardProjectionModel $card): void
    {
        $boardIndex = $this->getBoardIndexById($boardId);
        $board = BoardProjectionModel::fromData($this->data()['relationships']['boards'][$boardIndex]);

        $board->addCard($card);

        $newData = $this->data();
        $newData['relationships']['boards'][$boardIndex] = $board->data();

        $this->changeData($newData);
    }

    public function removeCardFromBoard(IdentityInterface $boardId, IdentityInterface $cardId): void
    {
        $boardIndex = $this->getBoardIndexById($boardId);
        $board = BoardProjectionModel::fromData($this->data()['relationships']['boards'][$boardIndex]);

        $board->removeCard($cardId);

        $newData = $this->data();
        $newData['relationships']['boards'][$boardIndex] = $board->data();

        $this->changeData($newData);
    }

    public function changeBoardCardText(IdentityInterface $boardId, IdentityInterface $cardId, Text $text): void
    {
        $boardIndex = $this->getBoardIndexById($boardId);
        $board = BoardProjectionModel::fromData($this->data()['relationships']['boards'][$boardIndex]);

        $board->changeCardText($cardId, $text);

        $newData = $this->data();
        $newData['relationships']['boards'][$boardIndex] = $board->data();

        $this->changeData($newData);
    }

    private function getBoardIndexById(IdentityInterface $boardId): int
    {
        $boardIdValue = $boardId->value();

        return $this->index(
            $this->data()['relationships']['boards'],
            function (array $data) use ($boardIdValue) {
                return $data['id'] === $boardIdValue;
            }
        );
    }
}
