<?php

declare(strict_types=1);

namespace RetroBoard\Domain\Meeting\WriteModel\Aggregate;

use RetroBoard\Domain\Meeting\WriteModel\Event\MeetingCreated;
use RetroBoard\Domain\Meeting\WriteModel\Event\MeetingNameChanged;
use RetroBoard\Domain\Meeting\WriteModel\ValueObject\Name;
use Zisato\EventSourcing\Aggregate\AbstractAggregateRoot;
use Zisato\EventSourcing\Identity\IdentityInterface;

class Meeting extends AbstractAggregateRoot
{
    private Name $name;

    public static function create(IdentityInterface $aggregateId, Name $name): self
    {
        $instance = new self($aggregateId);

        $instance->recordThat(MeetingCreated::create($aggregateId, $name));

        return $instance;
    }

    public function name(): Name
    {
        return clone $this->name;
    }

    public function changeName(Name $newName): void
    {
        if (! $this->name->equals($newName)) {
            $this->recordThat(MeetingNameChanged::create($this->id(), $this->name, $newName));
        }
    }

    public function applyMeetingCreated(MeetingCreated $event): void
    {
        $this->name = $event->name();
    }

    public function applyMeetingNameChanged(MeetingNameChanged $event): void
    {
        $this->name = $event->name();
    }
}
