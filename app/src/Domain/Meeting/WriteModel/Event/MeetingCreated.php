<?php

declare(strict_types=1);

namespace RetroBoard\Domain\Meeting\WriteModel\Event;

use RetroBoard\Domain\Meeting\WriteModel\ValueObject\Name;
use Zisato\EventSourcing\Aggregate\Event\AbstractEvent;
use Zisato\EventSourcing\Identity\IdentityInterface;

class MeetingCreated extends AbstractEvent
{
    private const DEFAULT_VERSION = 1;

    private const INDEX_NAME = 'name';

    public static function defaultVersion(): int
    {
        return static::DEFAULT_VERSION;
    }

    public static function create(IdentityInterface $aggregateId, Name $name): self
    {
        /** @var MeetingCreated $event */
        $event = self::occur($aggregateId->value(), [
            self::INDEX_NAME => $name->value(),
        ]);

        return $event;
    }

    public function name(): Name
    {
        return Name::fromValue($this->payload()[self::INDEX_NAME]);
    }
}
