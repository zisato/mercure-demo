<?php

declare(strict_types=1);

namespace RetroBoard\Domain\Meeting\WriteModel\Repository;

use RetroBoard\Domain\Meeting\WriteModel\Aggregate\Meeting;
use Zisato\EventSourcing\Aggregate\Repository\AggregateRootRepositoryInterface;
use Zisato\EventSourcing\Identity\IdentityInterface;

interface MeetingRepository extends AggregateRootRepositoryInterface
{
    public function get(IdentityInterface $aggregateId): Meeting;
}
