<?php

declare(strict_types=1);

namespace RetroBoard\Domain\Meeting\WriteModel\Service;

use RetroBoard\Domain\Meeting\WriteModel\Aggregate\Meeting;
use RetroBoard\Domain\Meeting\WriteModel\Repository\MeetingRepository;
use RetroBoard\Domain\Meeting\WriteModel\ValueObject\Name;
use Zisato\EventSourcing\Aggregate\Exception\AggregateRootNotFoundException;
use Zisato\EventSourcing\Aggregate\Exception\DuplicatedAggregateIdException;
use Zisato\EventSourcing\Identity\IdentityInterface;

class CreateMeetingService
{
    private MeetingRepository $meetingRepository;

    public function __construct(MeetingRepository $meetingRepository)
    {
        $this->meetingRepository = $meetingRepository;
    }

    public function create(IdentityInterface $meetingId, Name $name): void
    {
        $this->assertDuplicatedMeetingId($meetingId);

        $sprint = Meeting::create($meetingId, $name);

        $this->meetingRepository->save($sprint);
    }

    public function assertDuplicatedMeetingId(IdentityInterface $id): void
    {
        try {
            $this->meetingRepository->get($id);

            throw new DuplicatedAggregateIdException(\sprintf(
                'Aggregated id %s exists in repository.',
                $id->value()
            ));
        } catch (AggregateRootNotFoundException $exception) {
        }
    }
}
