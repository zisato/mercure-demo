<?php

declare(strict_types=1);

namespace RetroBoard\Infrastructure\Board\Controller;

use RetroBoard\Application\Board\Command\CreateBoard\CreateBoardCommand;
use Symfony\Component\HttpFoundation\Response;
use Zisato\ApiBundle\Infrastructure\Controller\AbstractCommandAPIController;
use Zisato\ApiBundle\Infrastructure\Service\RequestBodyServiceInterface;

class CreateBoardController extends AbstractCommandAPIController
{
    public function execute(RequestBodyServiceInterface $requestBodyService): Response
    {
        $requestData = $requestBodyService->requestBody('board/create.json');

        $command = new CreateBoardCommand(
            $requestData['data']['id'],
            $requestData['data']['attributes']['name'],
            $requestData['data']['relationships']['meeting']['id'],
            $requestData['data']['attributes']['background_color'] ?? null
        );

        $this->commandBus()
            ->handle($command);

        return $this->respondCreated();
    }
}
