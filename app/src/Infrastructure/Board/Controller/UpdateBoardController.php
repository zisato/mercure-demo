<?php

declare(strict_types=1);

namespace RetroBoard\Infrastructure\Board\Controller;

use RetroBoard\Application\Board\Command\UpdateBoard\UpdateBoardCommand;
use Symfony\Component\HttpFoundation\Response;
use Zisato\ApiBundle\Infrastructure\Controller\AbstractCommandAPIController;
use Zisato\ApiBundle\Infrastructure\Service\ArrayKeysParserService;
use Zisato\ApiBundle\Infrastructure\Service\RequestBodyServiceInterface;

class UpdateBoardController extends AbstractCommandAPIController
{
    public function execute(string $id, RequestBodyServiceInterface $requestBodyService): Response
    {
        $requestData = $requestBodyService->requestBody('board/update.json');
        $propertiesToUpdate = ArrayKeysParserService::arrayKeysAsDotNotation($requestData['data']);

        $command = new UpdateBoardCommand(
            $id,
            $propertiesToUpdate,
            $requestData['data']['attributes']['name'] ?? null,
            $requestData['data']['attributes']['background_color'] ?? null
        );

        $this->commandBus()
            ->handle($command);

        return $this->respondUpdated();
    }
}
