<?php

declare(strict_types=1);

namespace RetroBoard\Infrastructure\Board\WriteModel\Repository;

use RetroBoard\Domain\Board\WriteModel\Aggregate\Board;
use RetroBoard\Domain\Board\WriteModel\Repository\BoardRepository;
use Zisato\EventSourcing\Aggregate\Repository\AggregateRootRepositoryWithSnapshot;
use Zisato\EventSourcing\Identity\IdentityInterface;

class DBALBoardRepository extends AggregateRootRepositoryWithSnapshot implements BoardRepository
{
    public function get(IdentityInterface $aggregateId): Board
    {
        /** @var Board $result */
        $result = parent::get($aggregateId);

        return $result;
    }
}
