<?php

declare(strict_types=1);

namespace RetroBoard\Infrastructure\Card\Controller;

use RetroBoard\Application\Card\Command\CreateCard\CreateCardCommand;
use Symfony\Component\HttpFoundation\Response;
use Zisato\ApiBundle\Infrastructure\Controller\AbstractCommandAPIController;
use Zisato\ApiBundle\Infrastructure\Service\RequestBodyServiceInterface;

class CreateCardController extends AbstractCommandAPIController
{
    public function execute(RequestBodyServiceInterface $requestBodyService): Response
    {
        $requestData = $requestBodyService->requestBody('card/create.json');

        $command = new CreateCardCommand(
            $requestData['data']['id'],
            $requestData['data']['attributes']['text'],
            $requestData['data']['relationships']['board']['id']
        );

        $this->commandBus()
            ->handle($command);

        return $this->respondCreated();
    }
}
