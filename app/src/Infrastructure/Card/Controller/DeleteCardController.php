<?php

declare(strict_types=1);

namespace RetroBoard\Infrastructure\Card\Controller;

use RetroBoard\Application\Card\Command\DeleteCard\DeleteCardCommand;
use Symfony\Component\HttpFoundation\Response;
use Zisato\ApiBundle\Infrastructure\Controller\AbstractCommandAPIController;

class DeleteCardController extends AbstractCommandAPIController
{
    public function execute(string $id): Response
    {
        $command = new DeleteCardCommand($id);

        $this->commandBus()
            ->handle($command);

        return $this->respondDeleted();
    }
}
