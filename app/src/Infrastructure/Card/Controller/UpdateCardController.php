<?php

declare(strict_types=1);

namespace RetroBoard\Infrastructure\Card\Controller;

use RetroBoard\Application\Card\Command\UpdateCard\UpdateCardCommand;
use RetroBoard\Infrastructure\Common\Controller\UserTokenStorageController;
use Symfony\Component\HttpFoundation\Response;
use Zisato\ApiBundle\Infrastructure\Controller\AbstractCommandAPIController;
use Zisato\ApiBundle\Infrastructure\Service\ArrayKeysParserService;
use Zisato\ApiBundle\Infrastructure\Service\RequestBodyServiceInterface;

class UpdateCardController extends AbstractCommandAPIController
{
    public function execute(string $id, RequestBodyServiceInterface $requestBodyService): Response
    {
        $requestData = $requestBodyService->requestBody('card/update.json');
        $propertiesToUpdate = ArrayKeysParserService::arrayKeysAsDotNotation($requestData['data']);

        $command = new UpdateCardCommand(
            $id,
            $propertiesToUpdate,
            $requestData['data']['attributes']['text'],
            $requestData['data']['relationships']['board']
        );

        $this->commandBus()
            ->handle($command);

        return $this->respondUpdated();
    }
}
