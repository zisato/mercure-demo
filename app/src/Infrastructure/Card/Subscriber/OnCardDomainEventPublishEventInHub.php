<?php

declare(strict_types=1);

namespace RetroBoard\Infrastructure\Card\Subscriber;

use RetroBoard\Domain\Board\WriteModel\Repository\BoardRepository;
use RetroBoard\Domain\Card\WriteModel\Event\CardCreated;
use RetroBoard\Domain\Card\WriteModel\Event\CardDeleted;
use RetroBoard\Domain\Card\WriteModel\Event\CardTextChanged;
use RetroBoard\Domain\Card\WriteModel\Repository\CardRepository;
use RetroBoard\Domain\Meeting\ReadModel\ValueObject\CardProjectionModel;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;
use Zisato\ApiBundle\Infrastructure\EventSourcing\EventHandler;
use Zisato\ApiBundle\JSON\JSON;
use Zisato\EventSourcing\Aggregate\Event\EventInterface;
use Zisato\EventSourcing\Aggregate\Identity\UUID;

class OnCardDomainEventPublishEventInHub implements EventHandler
{
    private const CLASS_CODE_MAPPINGS = [
        CardCreated::class => 'CARD_CREATED',
        CardDeleted::class => 'CARD_DELETED',
        CardTextChanged::class => 'CARD_TEXT_CHANGED',
    ];

    private BoardRepository $boardRepository;

    private CardRepository $cardRepository;

    private HubInterface $publisher;

    public function __construct(
        BoardRepository $boardRepository,
        CardRepository $cardRepository,
        HubInterface $publisher
    ) {
        $this->boardRepository = $boardRepository;
        $this->cardRepository = $cardRepository;
        $this->publisher = $publisher;
    }

    public function __invoke(EventInterface $event): void
    {
        if (!in_array(get_class($event), array_keys(self::CLASS_CODE_MAPPINGS))) {
            return;
        }

        $cardId = UUID::fromString($event->aggregateId());

        $card = $this->cardRepository->get($cardId);
        $board = $this->boardRepository->get($card->boardId());

        $eventData = [
            'id' => $event->aggregateId(),
        ];

        if (! $event instanceof CardDeleted) {
            $card = $this->cardRepository->get($cardId);

            $eventData = CardProjectionModel::create(
                $cardId,
                $card->text(),
                $card->boardId(),
            )->data();
        }

        $data = [
            'event' => self::CLASS_CODE_MAPPINGS[\get_class($event)],
            'data' => $eventData,
        ];

        $update = new Update($board->id()->value(), JSON::encode($data));

        $this->publisher->publish($update);
    }
}
