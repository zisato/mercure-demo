<?php

declare(strict_types=1);

namespace RetroBoard\Infrastructure\Card\WriteModel\Repository;

use RetroBoard\Domain\Card\WriteModel\Aggregate\Card;
use RetroBoard\Domain\Card\WriteModel\Repository\CardRepository;
use Zisato\EventSourcing\Aggregate\Repository\AggregateRootRepositoryWithSnapshot;
use Zisato\EventSourcing\Identity\IdentityInterface;

class DBALCardRepository extends AggregateRootRepositoryWithSnapshot implements CardRepository
{
    public function get(IdentityInterface $aggregateId): Card
    {
        /** @var Card $result */
        $result = parent::get($aggregateId);

        return $result;
    }
}
