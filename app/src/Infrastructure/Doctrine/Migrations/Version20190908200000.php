<?php

declare(strict_types=1);

namespace RetroBoard\Infrastructure\Doctrine\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Zisato\EventSourcing\Infrastructure\Aggregate\Event\Store\DBALEventStore;
use Zisato\EventSourcing\Infrastructure\Aggregate\Snapshot\Store\DBALSnapshotStore;

class Version20190908200000 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()
                ->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(DBALEventStore::TABLE_SCHEMA);
        $this->addSql(DBALSnapshotStore::TABLE_SCHEMA);
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()
                ->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('DROP TABLE ' . DBALEventStore::TABLE_NAME);
        $this->addSql('DROP TABLE ' . DBALSnapshotStore::TABLE_NAME);
    }
}
