<?php

declare(strict_types=1);

namespace RetroBoard\Infrastructure\Meeting\Controller;

use RetroBoard\Application\Meeting\Command\CreateMeeting\CreateMeetingCommand;
use Symfony\Component\HttpFoundation\Response;
use Zisato\ApiBundle\Infrastructure\Controller\AbstractCommandAPIController;
use Zisato\ApiBundle\Infrastructure\Service\RequestBodyServiceInterface;

class CreateMeetingController extends AbstractCommandAPIController
{
    public function execute(RequestBodyServiceInterface $requestBodyService): Response
    {
        $requestData = $requestBodyService->requestBody('workbench/create.json');

        $command = new CreateMeetingCommand(
            $requestData['data']['id'],
            $requestData['data']['attributes']['name'],
        );

        $this->commandBus()
            ->handle($command);

        return $this->respondCreated();
    }
}
