<?php

declare(strict_types=1);

namespace RetroBoard\Infrastructure\Meeting\Controller;

use RetroBoard\Application\Meeting\Query\DetailMeeting\DetailMeetingQuery;
use Symfony\Component\HttpFoundation\Response;
use Zisato\ApiBundle\Infrastructure\Controller\AbstractQueryAPIController;

class DetailMeetingController extends AbstractQueryAPIController
{
    public function execute(string $id): Response
    {
        $query = new DetailMeetingQuery($id);

        $result = $this->queryBus()
            ->ask($query);

        return $this->respondSuccess($result->data());
    }
}
