<?php

declare(strict_types=1);

namespace RetroBoard\Infrastructure\Meeting\Controller;

use RetroBoard\Application\Meeting\Query\ListMeetings\ListMeetingsQuery;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Zisato\ApiBundle\Infrastructure\Controller\AbstractQueryAPIController;
use Zisato\ApiBundle\Infrastructure\Service\RequestQueryServiceInterface;
use Zisato\CQRS\ReadModel\ValueObject\ListableQueryResult;

class ListMeetingsController extends AbstractQueryAPIController
{
    public function execute(RequestStack $requestStack): Response
    {
        $query = ListMeetingsQuery::create(
            $requestStack->getCurrentRequest()->query->get('filter'),
            $requestStack->getCurrentRequest()->query->get('page'),
            $requestStack->getCurrentRequest()->query->get('perPage'),
            $requestStack->getCurrentRequest()->query->get('sortBy')
        );
        /** @var ListableQueryResult $result */
        $result = $this->queryBus()
            ->ask($query);

        return $this->respondCollection(
            $result->data(),
            $result->totalItems(),
            $result->page(),
            $result->perPage(),
            $result->totalPages(),
        );
    }
}
