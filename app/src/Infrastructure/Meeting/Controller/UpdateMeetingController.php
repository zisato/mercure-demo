<?php

declare(strict_types=1);

namespace RetroBoard\Infrastructure\Meeting\Controller;

use RetroBoard\Application\Meeting\Command\UpdateMeeting\UpdateMeetingCommand;
use Symfony\Component\HttpFoundation\Response;
use Zisato\ApiBundle\Infrastructure\Controller\AbstractCommandAPIController;
use Zisato\ApiBundle\Infrastructure\Service\ArrayKeysParserService;
use Zisato\ApiBundle\Infrastructure\Service\RequestBodyServiceInterface;

class UpdateMeetingController extends AbstractCommandAPIController
{
    public function execute(string $id, RequestBodyServiceInterface $requestBodyService): Response
    {
        $requestData = $requestBodyService->requestBody('workbench/update.json');
        $propertiesToUpdate = ArrayKeysParserService::arrayKeysAsDotNotation($requestData['data']);

        $command = new UpdateMeetingCommand(
            $id,
            $propertiesToUpdate,
            $requestData['data']['attributes']['name'] ?? null
        );

        $this->commandBus()
            ->handle($command);

        return $this->respondUpdated();
    }
}
