<?php

declare(strict_types=1);

namespace RetroBoard\Infrastructure\Meeting\ReadModel\Repository;

use RetroBoard\Domain\Meeting\ReadModel\Repository\MeetingsProjectionReadRepository;
use RetroBoard\Domain\Meeting\ReadModel\ValueObject\MeetingProjectionModel;
use Zisato\Projection\Infrastructure\MongoDB\Repository\MongoDBReadRepository;

class MongoDBMeetingsProjectionReadRepository extends MongoDBReadRepository implements MeetingsProjectionReadRepository
{
    public static function getProjectionModelName(): string
    {
        return MeetingProjectionModel::class;
    }

    public function getDatabaseName(): string
    {
        return 'retroboard';
    }

    public function getCollectionName(): string
    {
        return 'workbenchs';
    }

    public function get(string $id): MeetingProjectionModel
    {
        /** @var MeetingProjectionModel $result */
        $result = parent::get($id);

        return $result;
    }
}
