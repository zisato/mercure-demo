<?php

declare(strict_types=1);

namespace RetroBoard\Infrastructure\Meeting\ReadModel\Repository;

use RetroBoard\Domain\Meeting\ReadModel\Repository\MeetingsProjectionWriteRepository;
use Zisato\Projection\Infrastructure\MongoDB\Repository\MongoDBWriteRepository;

class MongoDBMeetingsProjectionWriteRepository extends MongoDBWriteRepository implements MeetingsProjectionWriteRepository
{
    public function getDatabaseName(): string
    {
        return 'retroboard';
    }

    public function getCollectionName(): string
    {
        return 'workbenchs';
    }
}
