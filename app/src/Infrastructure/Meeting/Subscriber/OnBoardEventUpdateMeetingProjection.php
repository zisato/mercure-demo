<?php

declare(strict_types=1);

namespace RetroBoard\Infrastructure\Meeting\Subscriber;

use RetroBoard\Domain\Board\WriteModel\Event\BoardBackgroundColorChanged;
use RetroBoard\Domain\Board\WriteModel\Event\BoardCreated;
use RetroBoard\Domain\Board\WriteModel\Event\BoardTitleChanged;
use RetroBoard\Domain\Board\WriteModel\Repository\BoardRepository;
use RetroBoard\Domain\Meeting\ReadModel\Repository\MeetingsProjectionReadRepository;
use RetroBoard\Domain\Meeting\ReadModel\Repository\MeetingsProjectionWriteRepository;
use RetroBoard\Domain\Meeting\ReadModel\ValueObject\BoardProjectionModel;
use RetroBoard\Domain\Meeting\ReadModel\ValueObject\MeetingProjectionModel;
use Zisato\ApiBundle\Infrastructure\EventSourcing\EventHandler;
use Zisato\EventSourcing\Aggregate\Event\EventInterface;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Identity\IdentityInterface;

class OnBoardEventUpdateMeetingProjection implements EventHandler
{
    private MeetingsProjectionReadRepository $meetingsProjectionReadRepository;

    private MeetingsProjectionWriteRepository $meetingsProjectionWriteRepository;

    private BoardRepository $boardRepository;

    public function __construct(
        MeetingsProjectionReadRepository $meetingsProjectionReadRepository,
        MeetingsProjectionWriteRepository $meetingsProjectionWriteRepository,
        BoardRepository $boardRepository
    ) {
        $this->meetingsProjectionReadRepository = $meetingsProjectionReadRepository;
        $this->meetingsProjectionWriteRepository = $meetingsProjectionWriteRepository;
        $this->boardRepository = $boardRepository;
    }

    function __invoke(EventInterface $event): void
    {
        if (!in_array(get_class($event), array_keys($this->getEventHandlers()))) {
            return;
        }

        $boardId = UUID::fromString($event->aggregateId());
        $meeting = $this->getMeetingByBoardId($boardId);

        $this->getEventHandlers()[get_class($event)]($meeting, $event);

        $this->meetingsProjectionWriteRepository->save($meeting);
    }

    private function getEventHandlers(): array
    {
        return [
            BoardCreated::class => function (MeetingProjectionModel $meeting, BoardCreated $event) {
                $meeting->addBoard(BoardProjectionModel::create(
                    UUID::fromString($event->aggregateId()),
                    $event->title(),
                    $event->backgroundColor(),
                    $event->meetingId()
                ));
            },
            BoardTitleChanged::class => function (MeetingProjectionModel $meeting, BoardTitleChanged $event) {
                $meeting->changeBoardTitle(UUID::fromString($event->aggregateId()), $event->title());
            },
            BoardBackgroundColorChanged::class => function (MeetingProjectionModel $meeting, BoardBackgroundColorChanged $event) {
                $meeting->changeBoardBackgroundColor(UUID::fromString($event->aggregateId()), $event->backgroundColor());
            },
        ];
    }

    private function getMeetingByBoardId(IdentityInterface $boardId): MeetingProjectionModel
    {
        $board = $this->boardRepository->get($boardId);

        return $this->meetingsProjectionReadRepository->get($board->meetingId() ->value());
    }
}
