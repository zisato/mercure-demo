<?php

declare(strict_types=1);

namespace RetroBoard\Infrastructure\Meeting\Subscriber;

use RetroBoard\Domain\Board\WriteModel\Repository\BoardRepository;
use RetroBoard\Domain\Card\WriteModel\Event\CardCreated;
use RetroBoard\Domain\Card\WriteModel\Event\CardDeleted;
use RetroBoard\Domain\Card\WriteModel\Event\CardTextChanged;
use RetroBoard\Domain\Card\WriteModel\Repository\CardRepository;
use RetroBoard\Domain\Meeting\ReadModel\Repository\MeetingsProjectionReadRepository;
use RetroBoard\Domain\Meeting\ReadModel\Repository\MeetingsProjectionWriteRepository;
use RetroBoard\Domain\Meeting\ReadModel\ValueObject\CardProjectionModel;
use RetroBoard\Domain\Meeting\ReadModel\ValueObject\MeetingProjectionModel;
use Zisato\ApiBundle\Infrastructure\EventSourcing\EventHandler;
use Zisato\EventSourcing\Aggregate\Event\EventInterface;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Identity\IdentityInterface;

class OnCardEventUpdateMeetingProjection implements EventHandler
{
    private MeetingsProjectionReadRepository $meetingsProjectionReadRepository;

    private MeetingsProjectionWriteRepository $meetingsProjectionWriteRepository;

    private CardRepository $cardRepository;

    private BoardRepository $boardRepository;

    public function __construct(
        MeetingsProjectionReadRepository $meetingsProjectionReadRepository,
        MeetingsProjectionWriteRepository $meetingsProjectionWriteRepository,
        CardRepository $cardRepository,
        BoardRepository $boardRepository
    ) {
        $this->meetingsProjectionReadRepository = $meetingsProjectionReadRepository;
        $this->meetingsProjectionWriteRepository = $meetingsProjectionWriteRepository;
        $this->cardRepository = $cardRepository;
        $this->boardRepository = $boardRepository;
    }

    function __invoke(EventInterface $event): void
    {
        if (!in_array(get_class($event), array_keys($this->getEventHandlers()))) {
            return;
        }

        $cardId = UUID::fromString($event->aggregateId());
        $meeting = $this->getMeetingByCardId($cardId);

        $this->getEventHandlers()[get_class($event)]($meeting, $event);

        $this->meetingsProjectionWriteRepository->save($meeting);
    }

    private function getEventHandlers(): array
    {
        return [
            CardCreated::class => function (MeetingProjectionModel $meeting, CardCreated $event) {
                $card = CardProjectionModel::create(UUID::fromString($event->aggregateId()), $event->text(), $event->boardId());
    
                $meeting->addCardToBoard($event->boardId(), $card);
            },
            CardDeleted::class => function (MeetingProjectionModel $meeting, CardDeleted $event) {
                $meeting->removeCardFromBoard($event->boardId(), UUID::fromString($event->aggregateId()));
            },
            CardTextChanged::class => function (MeetingProjectionModel $meeting, CardTextChanged $event) {
                $meeting->changeBoardCardText($event->boardId(), UUID::fromString($event->aggregateId()), $event->text());
            },
        ];
    }

    private function getMeetingByCardId(IdentityInterface $cardId): MeetingProjectionModel
    {
        $card = $this->cardRepository->get($cardId);
        $board = $this->boardRepository->get($card->boardId());

        return $this->meetingsProjectionReadRepository->get($board->meetingId() ->value());
    }
}
