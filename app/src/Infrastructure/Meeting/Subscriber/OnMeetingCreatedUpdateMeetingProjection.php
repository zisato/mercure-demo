<?php

declare(strict_types=1);

namespace RetroBoard\Infrastructure\Meeting\Subscriber;

use RetroBoard\Domain\Meeting\ReadModel\Repository\MeetingsProjectionWriteRepository;
use RetroBoard\Domain\Meeting\ReadModel\ValueObject\MeetingProjectionModel;
use RetroBoard\Domain\Meeting\WriteModel\Event\MeetingCreated;
use Zisato\ApiBundle\Infrastructure\EventSourcing\EventHandler;
use Zisato\EventSourcing\Aggregate\Identity\UUID;

class OnMeetingCreatedUpdateMeetingProjection implements EventHandler
{
    private MeetingsProjectionWriteRepository $meetingsProjectionWriteRepository;

    public function __construct(MeetingsProjectionWriteRepository $meetingsProjectionWriteRepository)
    {
        $this->meetingsProjectionWriteRepository = $meetingsProjectionWriteRepository;
    }

    public function __invoke(MeetingCreated $event): void
    {
        $meeting = MeetingProjectionModel::create(UUID::fromString($event->aggregateId()), $event->name());

        $this->meetingsProjectionWriteRepository->save($meeting);
    }
}
