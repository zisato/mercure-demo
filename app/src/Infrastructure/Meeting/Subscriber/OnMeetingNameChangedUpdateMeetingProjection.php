<?php

declare(strict_types=1);

namespace RetroBoard\Infrastructure\Meeting\Subscriber;

use RetroBoard\Domain\Meeting\ReadModel\Repository\MeetingsProjectionReadRepository;
use RetroBoard\Domain\Meeting\ReadModel\Repository\MeetingsProjectionWriteRepository;
use RetroBoard\Domain\Meeting\WriteModel\Event\MeetingNameChanged;
use Zisato\ApiBundle\Infrastructure\EventSourcing\EventHandler;

class OnMeetingNameChangedUpdateMeetingProjection implements EventHandler
{
    private MeetingsProjectionReadRepository $meetingsProjectionReadRepository;

    private MeetingsProjectionWriteRepository $meetingsProjectionWriteRepository;

    public function __construct(
        MeetingsProjectionReadRepository $meetingsProjectionReadRepository,
        MeetingsProjectionWriteRepository $meetingsProjectionWriteRepository
    ) {
        $this->meetingsProjectionReadRepository = $meetingsProjectionReadRepository;
        $this->meetingsProjectionWriteRepository = $meetingsProjectionWriteRepository;
    }

    public function __invoke(MeetingNameChanged $event): void
    {
        $meeting = $this->meetingsProjectionReadRepository->get($event->aggregateId());

        $meeting->changeName($event->name());

        $this->meetingsProjectionWriteRepository->save($meeting);
    }
}
