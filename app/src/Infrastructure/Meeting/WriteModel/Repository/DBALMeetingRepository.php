<?php

declare(strict_types=1);

namespace RetroBoard\Infrastructure\Meeting\WriteModel\Repository;

use RetroBoard\Domain\Meeting\WriteModel\Aggregate\Meeting;
use RetroBoard\Domain\Meeting\WriteModel\Repository\MeetingRepository;
use Zisato\EventSourcing\Aggregate\Repository\AggregateRootRepositoryWithSnapshot;
use Zisato\EventSourcing\Identity\IdentityInterface;

class DBALMeetingRepository extends AggregateRootRepositoryWithSnapshot implements MeetingRepository
{
    public function get(IdentityInterface $aggregateId): Meeting
    {
        /** @var Meeting $result */
        $result = parent::get($aggregateId);

        return $result;
    }
}
