<?php

declare(strict_types=1);

namespace Zisato\ApiBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Zisato\ApiBundle\DependencyInjection\Compiler\AggregateAutoConfigPass;
use Zisato\ApiBundle\DependencyInjection\Compiler\AggregateConfigDefinitionPass;
use Zisato\ApiBundle\DependencyInjection\Compiler\EventDecoratorPass;

class ApiBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new AggregateAutoConfigPass());
        $container->addCompilerPass(new AggregateConfigDefinitionPass());
        // $container->addCompilerPass(new EventDecoratorPass());
    }
}
