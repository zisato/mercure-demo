<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\Application\ReadModel\ValueObject\Filters;

use Zisato\Projection\Criteria\Condition;

class Filter
{
    private string $column;

    private Type $type;

    private Condition $condition;

    public function __construct(string $column, Type $type, ?Condition $condition = null)
    {
        $this->column = $column;
        $this->type = $type;
        $this->condition = $condition ?? Condition::eq();
    }

    public function column(): string
    {
        return $this->column;
    }

    public function type(): Type
    {
        return $this->type;
    }

    public function condition(): Condition
    {
        return $this->condition;
    }
}
