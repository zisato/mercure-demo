<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\Application\ReadModel\ValueObject\Filters;

class FilterCollection
{
    /**
     * @var array<string, Filter>
     */
    private array $values = [];

    public function __construct(Filter ...$filters)
    {
        foreach ($filters as $filter) {
            $this->add($filter);
        }
    }

    /**
     * @return iterable<Filter>
     */
    public function values(): iterable
    {
        foreach ($this->values as $value) {
            yield $value;
        }
    }

    public function add(Filter $filter): void
    {
        $this->values[$filter->column()] = $filter;
    }

    public function get(string $column): Filter
    {
        if (! $this->exists($column)) {
            throw new \InvalidArgumentException('Filter column not found.');
        }

        return $this->values[$column];
    }

    public function exists(string $column): bool
    {
        return \array_key_exists($column, $this->values);
    }
}
