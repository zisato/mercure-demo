<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\Application\ReadModel\ValueObject\Filters;

class Type
{
    public const TYPE_BOOLEAN = 'boolean';

    public const TYPE_FLOAT = 'float';

    public const TYPE_INTEGER = 'integer';

    public const TYPE_STRING = 'string';

    private const VALID_VALUES = [self::TYPE_BOOLEAN, self::TYPE_FLOAT, self::TYPE_INTEGER, self::TYPE_STRING];

    private const FILTER_VARS = [
        self::TYPE_BOOLEAN => FILTER_VALIDATE_BOOLEAN,
        self::TYPE_FLOAT => FILTER_VALIDATE_FLOAT,
        self::TYPE_INTEGER => FILTER_VALIDATE_INT,
        self::TYPE_STRING => FILTER_DEFAULT,
    ];

    private string $value;

    final private function __construct(string $value)
    {
        $this->checkValidValue($value);

        $this->value = $value;
    }

    public static function create(string $value): self
    {
        return new static($value);
    }

    public static function boolean(): self
    {
        return new static(self::TYPE_BOOLEAN);
    }

    public static function float(): self
    {
        return new static(self::TYPE_FLOAT);
    }

    public static function integer(): self
    {
        return new static(self::TYPE_INTEGER);
    }

    public static function string(): self
    {
        return new static(self::TYPE_STRING);
    }

    public function value(): string
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    public function filterValue($value)
    {
        return \filter_var($value, self::FILTER_VARS[$this->value]);
    }

    private function checkValidValue(string $value): void
    {
        if (! \in_array($value, self::VALID_VALUES, true)) {
            throw new \InvalidArgumentException(\sprintf(
                'Invalid filter type %s. Allowed types are: %s',
                $value,
                \implode(', ', self::VALID_VALUES)
            ));
        }
    }
}
