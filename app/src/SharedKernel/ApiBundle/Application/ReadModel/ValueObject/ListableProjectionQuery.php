<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\Application\ReadModel\ValueObject;

use Zisato\ApiBundle\Application\ReadModel\ValueObject\Filters\Filter;
use Zisato\ApiBundle\Application\ReadModel\ValueObject\Filters\FilterCollection;
use Zisato\ApiBundle\Application\ReadModel\ValueObject\SortBy\SortBy;
use Zisato\ApiBundle\Application\ReadModel\ValueObject\SortBy\SortByCollection;
use Zisato\CQRS\ReadModel\ValueObject\ListableQuery;
use Zisato\Projection\Criteria\Condition;
use Zisato\Projection\Criteria\Criteria;
use Zisato\Projection\OrderBy\Direction;
use Zisato\Projection\OrderBy\OrderBy;

abstract class ListableProjectionQuery implements ListableQuery
{
    public const DEFAULT_PAGE = 1;

    public const DEFAULT_PER_PAGE = 20;

    /**
     * @param array<string, mixed> $filters
     */
    private array $filters;

    private int $page;

    private int $perPage;

    /**
     * @param array<string, string> $sortBy
     */
    private array $sortBy;

    /**
     * @param array<string, mixed>|null $filters
     * @param array<string, string>|null $sortBy
     */
    final protected function __construct(array $filters, int $page, int $perPage, array $sortBy)
    {
        $this->assertInvalidFilters($filters);
        $this->assertInvalidSortBy($sortBy);

        $this->filters = $filters;
        $this->page = $page;
        $this->perPage = $perPage;
        $this->sortBy = $sortBy;
    }

    /**
     * @param array<string, mixed>|null $filters
     * @param array<string, string>|null $sortBy
     */
    public static function create(
        ?array $filters = null,
        ?int $page = null,
        ?int $perPage = null,
        ?array $sortBy = null
    ): ListableQuery {
        return new static(
            $filters ?? [],
            $page ?? static::DEFAULT_PAGE,
            $perPage ?? static::DEFAULT_PER_PAGE,
            $sortBy ?? []
        );
    }

    public function page(): int
    {
        return $this->page;
    }

    public function perPage(): int
    {
        return $this->perPage;
    }

    /**
     * @return array<string, mixed>
     */
    public function filters(): array
    {
        return $this->filters;
    }

    /**
     * @return array<string, string>
     */
    public function sortBy(): array
    {
        return $this->sortBy;
    }

    public function criteria(): Criteria
    {
        $criteria = new Criteria();
        $filterDefinitions = $this->allowedFilters();

        foreach ($this->filters() as $column => $value) {
            $filterDefinition = $filterDefinitions->get($column);

            $criteria->add(
                $filterDefinition->column(),
                $filterDefinition->type()->filterValue($value),
                $filterDefinition->condition()
            );
        }

        return $criteria;
    }

    public function orderBy(): OrderBy
    {
        $orderBy = new OrderBy();
        $allowedSortBy = $this->allowedSortBy();

        foreach ($this->sortBy() as $column => $direction) {
            $sortBy = $allowedSortBy->get($column);

            $orderBy->add($sortBy->column(), new Direction($direction));
        }

        return $orderBy;
    }

    abstract protected function allowedFilters(): FilterCollection;

    abstract protected function allowedSortBy(): SortByCollection;

    /**
     * @param array<string, mixed> $filters
     */
    private function assertInvalidFilters(array $filters): void
    {
        $allowedFilters = $this->allowedFilters();

        foreach (array_keys($filters) as $column) {
            if (! $allowedFilters->exists($column)) {
                throw new \InvalidArgumentException(
                    \sprintf('Filter column %s not exists in allowed filters.', $column)
                );
            }
        }
    }

    /**
     * @param array<string, string> $sortBy
     */
    private function assertInvalidSortBy(array $sortBy): void
    {
        $allowedSortBy = $this->allowedSortBy();

        foreach (array_keys($sortBy) as $column) {
            if (! $allowedSortBy->exists($column)) {
                throw new \InvalidArgumentException(
                    \sprintf('SortBy column %s not exists in allowed sortBy.', $column)
                );
            }
        }
    }
}
