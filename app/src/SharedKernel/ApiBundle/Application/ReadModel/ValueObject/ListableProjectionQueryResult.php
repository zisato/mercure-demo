<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\Application\ReadModel\ValueObject;

use Zisato\CQRS\ReadModel\ValueObject\ListableQueryResult;
use Zisato\Projection\Transformer\ProjectionModelTransformer;
use Zisato\Projection\ValueObject\ProjectionModelCollection;

class ListableProjectionQueryResult implements ListableQueryResult
{
    /**
     * @var array<string|int, mixed>
     */
    private array $data;

    private int $page;

    private int $perPage;

    private int $totalPages;

    private int $totalItems;

    final protected function __construct(
        ProjectionModelCollection $collection,
        ProjectionModelTransformer $transformer,
        int $totalItems,
        int $page,
        int $perPage
    ) {
        $this->data = [];
        foreach ($collection->data() as $projectionModel) {
            $this->data[] = $transformer->transform($projectionModel);
        }
        $this->page = $page;
        $this->perPage = $perPage;
        $this->totalPages = (int) \ceil($totalItems / $perPage);
        $this->totalItems = $totalItems;
    }

    public static function create(
        ProjectionModelCollection $collection,
        ProjectionModelTransformer $transformer,
        int $totalItems,
        int $page,
        int $perPage
    ): ListableQueryResult {
        return new static($collection, $transformer, $totalItems, $page, $perPage);
    }

    public function data(): array
    {
        return $this->data;
    }

    public function page(): int
    {
        return $this->page;
    }

    public function perPage(): int
    {
        return $this->perPage;
    }

    public function totalPages(): int
    {
        return $this->totalPages;
    }

    public function totalItems(): int
    {
        return $this->totalItems;
    }
}
