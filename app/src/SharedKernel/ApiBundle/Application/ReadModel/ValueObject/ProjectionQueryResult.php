<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\Application\ReadModel\ValueObject;

use Zisato\CQRS\ReadModel\ValueObject\QueryResult;
use Zisato\Projection\Transformer\ProjectionModelTransformer;
use Zisato\Projection\ValueObject\ProjectionModel;

class ProjectionQueryResult implements QueryResult
{
    /**
     * @var array<string|int, mixed>
     */
    private array $data;

    final protected function __construct(ProjectionModel $data, ProjectionModelTransformer $transformer)
    {
        $this->data = $transformer->transform($data);
    }

    public static function create(ProjectionModel $data, ProjectionModelTransformer $transformer): QueryResult
    {
        return new static($data, $transformer);
    }

    public function data(): array
    {
        return $this->data;
    }
}
