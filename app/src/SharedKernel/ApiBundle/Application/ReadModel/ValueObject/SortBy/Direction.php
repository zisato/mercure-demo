<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\Application\ReadModel\ValueObject\SortBy;

class Direction
{
    public const DIRECTION_ASC = 'asc';

    public const DIRECTION_DESC = 'desc';

    public const VALID_VALUES = [self::DIRECTION_ASC, self::DIRECTION_DESC];

    public static function isValid(string $value): bool
    {
        return \in_array($value, self::VALID_VALUES, true);
    }
}
