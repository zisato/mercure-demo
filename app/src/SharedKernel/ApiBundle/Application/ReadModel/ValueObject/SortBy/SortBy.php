<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\Application\ReadModel\ValueObject\SortBy;

class SortBy
{
    private string $column;

    public function __construct(string $column)
    {
        $this->column = $column;
    }

    public function column(): string
    {
        return $this->column;
    }
}
