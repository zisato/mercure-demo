<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\Application\ReadModel\ValueObject\SortBy;

class SortByCollection
{
    /**
     * @var array<string, SortBy>
     */
    private array $values = [];

    public function __construct(SortBy ...$sortBy)
    {
        foreach ($sortBy as $value) {
            $this->add($value);
        }
    }

    /**
     * @return iterable<SortBy>
     */
    public function values(): iterable
    {
        foreach ($this->values as $value) {
            yield $value;
        }
    }

    public function add(SortBy $sortBy): void
    {
        $this->values[$sortBy->column()] = $sortBy;
    }

    public function get(string $column): SortBy
    {
        if (! $this->exists($column)) {
            throw new \InvalidArgumentException('SortBy column not found.');
        }

        return $this->values[$column];
    }

    public function exists(string $column): bool
    {
        return \array_key_exists($column, $this->values);
    }
}
