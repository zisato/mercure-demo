<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\PhpFileLoader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\ConfigurableExtension;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\Transport\Doctrine\DoctrineTransportFactory;
use Zisato\ApiBundle\Infrastructure\EventSourcing\EventHandler;
use Zisato\ApiBundle\Infrastructure\ExceptionHandler\Strategy\ExceptionHandlerStrategyChain;
use Zisato\ApiBundle\Infrastructure\Symfony\EventListener\APIProblemExceptionListener;
use Zisato\ApiBundle\Infrastructure\Symfony\MessageHandler\Middleware\DBALTransactionMiddleware;
use Zisato\CQRS\ReadModel\Service\QueryHandler;
use Zisato\CQRS\WriteModel\Service\CommandHandler;
use Zisato\EventSourcing\Aggregate\AbstractAggregateRoot;
use Zisato\EventSourcing\Aggregate\Event\Decorator\EventDecoratorInterface;
use Zisato\EventSourcing\Aggregate\Event\EventInterface;
use Zisato\EventSourcing\Aggregate\Event\Upcast\UpcasterInterface;
use Zisato\EventSourcing\Aggregate\Event\Version\VersionResolverInterface;
use Zisato\EventSourcing\Aggregate\Repository\AggregateRootRepository;
use Zisato\EventSourcing\Aggregate\Repository\AggregateRootRepositoryWithSnapshot;
use Zisato\EventSourcing\Aggregate\Snapshot\Service\SnapshotServiceInterface;
use Zisato\EventSourcing\Aggregate\Snapshot\Strategy\SnapshotStrategyInterface;

class ApiExtension extends ConfigurableExtension implements PrependExtensionInterface
{
    public const BUS_COMMANDS = 'command.bus';

    public const BUS_EVENTS = 'event.bus';

    public const BUS_QUERIES = 'query.bus';

    public const TAG_AGGREGATE_ROOT = 'api_bundle.aggregate_root';

    public const TAG_AGGREGATE_ROOT_REPOSITORY = 'api_bundle.aggregate_root.repository';

    public const TAG_AGGREGATE_ROOT_REPOSITORY_WITH_SNAPSHOT = 'api_bundle.aggregate_root.repository_with_snapshot';

    public const TAG_EVENT_DECORATOR = 'api_bundle.aggregate_root.event.decorator';

    public const TAG_EVENT_UPCASTER = 'api_bundle.aggregate_root.event.upcaster';

    private const SYMFONY_KERNEL_EVENT_LISTENER = 'kernel.event_listener';

    private const SYMFONY_KERNEL_EVENT_SUBSCRIBER = 'kernel.event_subscriber';

    private const SYMFONY_MESSENGER_MESSAGE_HANDLER = 'messenger.message_handler';

    private const SYMFONY_MESSENGER_TRANSPORT_FACTORY = 'messenger.transport_factory';

    public function prepend(ContainerBuilder $container): void
    {
        $container->prependExtensionConfig('framework', [
            'messenger' => [
                'default_bus' => self::BUS_COMMANDS,
                'buses' => [
                    self::BUS_COMMANDS => [
                        'default_middleware' => false,
                        'middleware' => [
                            'dispatch_after_current_bus',
                            DBALTransactionMiddleware::class,
                            'handle_message',
                        ],
                    ],
                    self::BUS_EVENTS => [
                        'default_middleware' => 'allow_no_handlers',
                    ],
                    self::BUS_QUERIES => [
                        'default_middleware' => false,
                        'middleware' => ['handle_message'],
                    ],
                ],
                'routing' => [
                    EventInterface::class => 'async',
                ],
            ],
        ]);
    }

    public function loadInternal(array $mergedConfig, ContainerBuilder $container)
    {
        $loader = new PhpFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));

        $loader->load('api_bundle.php');
        $loader->load('cqrs_symfony_messenger.php');
        $loader->load('event_sourcing.php');
        $loader->load('event_sourcing_snapshot.php');

        $container->setParameter('api_bundle.json_schema_path', $mergedConfig['json_schema_path']);
        $container->setParameter('api_bundle.aggregates', $mergedConfig['aggregates']);

        $container->setAlias(VersionResolverInterface::class, $mergedConfig['event']['version_resolver']);
        $container->setAlias(SnapshotStrategyInterface::class, $mergedConfig['snapshot']['strategy']);
        $container->setAlias(SnapshotServiceInterface::class, $mergedConfig['snapshot']['service']);

        if (! empty($mergedConfig['exception_handlers'])) {
            $this->mergeExceptionHandlerConfig($container, $mergedConfig['exception_handlers']);
        }

        $container->findDefinition(APIProblemExceptionListener::class)
            ->addTag(self::SYMFONY_KERNEL_EVENT_LISTENER, [
                'event' => 'kernel.exception',
                'method' => 'onKernelException',
            ]);

        $container->findDefinition(DoctrineTransportFactory::class)
            ->addTag(self::SYMFONY_MESSENGER_TRANSPORT_FACTORY);

        $container->registerForAutoconfiguration(EventHandler::class)
            ->addTag(self::SYMFONY_MESSENGER_MESSAGE_HANDLER, [
                'bus' => self::BUS_EVENTS,
            ]);

        $container->registerForAutoconfiguration(CommandHandler::class)
            ->addTag(self::SYMFONY_MESSENGER_MESSAGE_HANDLER, [
                'bus' => self::BUS_COMMANDS,
            ]);

        $container->registerForAutoconfiguration(QueryHandler::class)
            ->addTag(self::SYMFONY_MESSENGER_MESSAGE_HANDLER, [
                'bus' => self::BUS_QUERIES,
            ]);

        $container->registerForAutoconfiguration(EventDecoratorInterface::class)
            ->addTag(self::TAG_EVENT_DECORATOR);

        $container->registerForAutoconfiguration(AggregateRootRepository::class)
            ->addTag(self::TAG_AGGREGATE_ROOT_REPOSITORY);

        $container->registerForAutoconfiguration(AggregateRootRepositoryWithSnapshot::class)
            ->addTag(self::TAG_AGGREGATE_ROOT_REPOSITORY);

        $container->registerForAutoconfiguration(UpcasterInterface::class)
            ->addTag(self::TAG_EVENT_UPCASTER);

        $container->registerForAutoconfiguration(AbstractAggregateRoot::class)
            ->addTag(self::TAG_AGGREGATE_ROOT);
    }

    private function mergeExceptionHandlerConfig(ContainerBuilder $container, array $exceptionHandlers): void
    {
        $configArguments = array_map(function (string $id) {
            return new Reference($id);
        }, $exceptionHandlers);

        $definition = $container->findDefinition(ExceptionHandlerStrategyChain::class);

        $newArguments = array_merge($configArguments, $definition->getArguments());

        $definition->setArguments($newArguments);
    }
}
