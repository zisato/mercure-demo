<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Zisato\ApiBundle\DependencyInjection\ApiExtension;
use Zisato\EventSourcing\Aggregate\AggregateRootInterface;
use Zisato\EventSourcing\Aggregate\Event\EventInterface;

class AggregateAutoConfigPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        $repositoryServiceIds = array_keys(
            $container->findTaggedServiceIds(ApiExtension::TAG_AGGREGATE_ROOT_REPOSITORY, true)
        );
        $upcastersGroupedByEventName = $this->upcastersGroupedByEventName($container);

        $aggregates = [];

        foreach ($repositoryServiceIds as $repositoryServiceId) {
            $repositoryReflection = $container->getReflectionClass($repositoryServiceId);
            $returnType = $repositoryReflection->getMethod('get')
                ->getReturnType();
            if ($returnType === null) {
                continue;
            }

            $aggregateClass = $returnType->getName();
            $aggregateReflection = $container->getReflectionClass($aggregateClass);

            if ($this->isValidAggregateRoot($aggregateReflection)) {
                $upcasters = $this->aggregateRootEventUpcasters($aggregateReflection, $upcastersGroupedByEventName);

                $aggregates[] = [
                    'class' => $aggregateClass,
                    'repository' => $repositoryServiceId,
                    'upcasters' => $upcasters,
                ];
            }
        }

        $container->setParameter('api_bundle.aggregates', $aggregates);
    }

    private function upcastersGroupedByEventName(ContainerBuilder $container): array
    {
        $upcasterServiceIds = $container->findTaggedServiceIds(ApiExtension::TAG_EVENT_UPCASTER, true);
        $upcasterGroups = [];

        foreach ($upcasterServiceIds as $id => $service) {
            $reflection = $container->getReflectionClass($id);

            $method = $reflection->getMethod('upcast');
            $returnTypeName = $method->getReturnType()
                ->getName();

            if ($returnTypeName !== EventInterface::class
                && ! ($reflection = $container->getReflectionClass($returnTypeName))
                    ->isAbstract()
                && $reflection->implementsInterface(EventInterface::class)
            ) {
                $upcasterGroups[$returnTypeName][] = $id;
            }
        }

        return $upcasterGroups;
    }

    private function isValidAggregateRoot(\ReflectionClass $aggregateReflection): bool
    {
        return $aggregateReflection->implementsInterface(AggregateRootInterface::class) &&
            ! $aggregateReflection->isAbstract();
    }

    private function aggregateRootEventUpcasters(
        \ReflectionClass $aggregateReflection,
        array $upcastersGroupedByEventName
    ): array {
        $upcasters = [];

        foreach ($aggregateReflection->getMethods() as $method) {
            if ($this->aggregateRootHasApplyEventMethod($method)) {
                $eventName = $method->getParameters()[0]
                    ->getType()
                    ->getName();

                $upcasters = array_merge($upcasters, $upcastersGroupedByEventName[$eventName] ?? []);
            }
        }

        return $upcasters;
    }

    private function aggregateRootHasApplyEventMethod(\ReflectionMethod $method): bool
    {
        $prefix = 'apply';
        $methodName = $method->getName();

        return substr($methodName, 0, strlen($prefix)) === $prefix
            && strlen($methodName) > strlen($prefix)
            && $method->getNumberOfParameters() === 1
            && ! ($r = new \ReflectionClass($method->getParameters()[0]->getType()->getName()))->isAbstract()
            && $r->implementsInterface(EventInterface::class);
    }
}
