<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\DependencyInjection\Compiler;

use Doctrine\DBAL\Connection;
use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Zisato\ApiBundle\Infrastructure\EventSourcing\Aggregate\Event\Upcast\EventUpcasterChain;
use Zisato\ApiBundle\Infrastructure\EventSourcing\Aggregate\Event\Upcast\EventUpcasterGroup;
use Zisato\ApiBundle\Infrastructure\EventSourcing\Aggregate\Event\Upcast\EventUpcasterGroupCollection;
use Zisato\EventSourcing\Aggregate\Event\Bus\EventBusInterface;
use Zisato\EventSourcing\Aggregate\Event\Decorator\EventDecoratorInterface;
use Zisato\EventSourcing\Aggregate\Event\EventInterface;
use Zisato\EventSourcing\Aggregate\Event\Serializer\EventSerializerInterface;
use Zisato\EventSourcing\Aggregate\Event\Serializer\UpcasterEventSerializer;
use Zisato\EventSourcing\Aggregate\Repository\AggregateRootRepository;
use Zisato\EventSourcing\Aggregate\Repository\AggregateRootRepositoryWithSnapshot;
use Zisato\EventSourcing\Aggregate\Snapshot\SnapshotterInterface;
use Zisato\EventSourcing\Infrastructure\Aggregate\Event\Store\DBALEventStore;

class AggregateConfigDefinitionPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        $aggregates = $container->getParameter('api_bundle.aggregates');

        foreach ($aggregates as $aggregateConfig) {
            if (! $container->findDefinition($aggregateConfig['repository'])) {
                continue;
            }

            $this->createEventSerializer($aggregateConfig, $container);
            $this->createEventStore($aggregateConfig, $container);
            $this->createRepository($aggregateConfig, $container);
        }
    }

    private function createEventSerializer(array $config, ContainerBuilder $container): void
    {
        $eventSerializerDefinition = new ChildDefinition(EventSerializerInterface::class);

        if (count($config['upcasters']) > 0) {
            $eventSerializerDefinition = $this->createUpcasterEventSerializer(
                $eventSerializerDefinition,
                $config['upcasters']
            );
        }

        $container->setDefinition(
            \sprintf('api_bundle.event_serializer.%s', $config['class']),
            $eventSerializerDefinition
        );
    }

    private function createEventStore(array $config, ContainerBuilder $container): void
    {
        $eventStoreDefinition = new Definition(DBALEventStore::class);
        $eventStoreDefinition
            ->setArguments(
                [
                    new Reference(Connection::class),
                    new Reference(\sprintf('api_bundle.event_serializer.%s', $config['class'])),
                ]
            );

        $container->setDefinition(\sprintf('api_bundle.event_store.%s', $config['class']), $eventStoreDefinition);
    }

    private function createRepository(array $config, ContainerBuilder $container): void
    {
        $repositoryReflection = $container->getReflectionClass($config['repository']);

        if ($repositoryReflection->isSubclassOf(AggregateRootRepository::class)) {
            $repositoryDefinition = $container->findDefinition($config['repository']);
            $repositoryDefinition->setArguments([
                $config['class'],
                new Reference(\sprintf('api_bundle.event_store.%s', $config['class'])),
                new Reference(EventDecoratorInterface::class),
                new Reference(EventBusInterface::class),
            ]);
        }

        if ($repositoryReflection->isSubclassOf(AggregateRootRepositoryWithSnapshot::class)) {
            $repositoryDefinition = new Definition(AggregateRootRepository::class);
            $repositoryDefinition->setArguments([
                $config['class'],
                new Reference(\sprintf('api_bundle.event_store.%s', $config['class'])),
                new Reference(EventDecoratorInterface::class),
                new Reference(EventBusInterface::class),
            ]);

            $repositoryWithSnapshotDefinition = $container->findDefinition($config['repository']);
            $repositoryWithSnapshotDefinition->setArguments([
                $repositoryDefinition,
                new Reference(\sprintf('api_bundle.event_store.%s', $config['class'])),
                new Reference(SnapshotterInterface::class),
            ]);
        }
    }

    private function createUpcasterEventSerializer(Definition $eventSerializerDefinition, array $upcasters): Definition
    {
        $upcastersGroupedByEventName = [];

        foreach ($upcasters as $id) {
            $reflection = new \ReflectionClass($id);

            $method = $reflection->getMethod('upcast');
            $returnTypeName = $method->getReturnType()
                ->getName();

            if ($returnTypeName !== Event::class
                && ! ($reflection = new \ReflectionClass($returnTypeName))
                    ->isAbstract()
                && $reflection->implementsInterface(EventInterface::class)
            ) {
                $upcastersGroupedByEventName[$returnTypeName][] = new Reference($id);
            }
        }

        $upcasterGroups = [];
        foreach ($upcastersGroupedByEventName as $eventName => $upcasters) {
            $upcasterGroups[] = new Definition(EventUpcasterGroup::class, [$eventName, ...$upcasters]);
        }

        $upcasterChain = new Definition(EventUpcasterChain::class, [
            new Definition(EventUpcasterGroupCollection::class, $upcasterGroups),
        ]);

        return new Definition(UpcasterEventSerializer::class, [$eventSerializerDefinition, $upcasterChain]);
    }
}
