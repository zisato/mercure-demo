<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Zisato\ApiBundle\DependencyInjection\ApiExtension;
use Zisato\EventSourcing\Aggregate\Event\Decorator\EventDecoratorChain;

class EventDecoratorPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if (! $container->hasDefinition(EventDecoratorChain::class)) {
            return;
        }

        $eventDecorators = \array_keys($container->findTaggedServiceIds(ApiExtension::TAG_EVENT_DECORATOR, true));

        $eventDecoratorDefinitions = \array_map(function (string $eventDecorator) {
            return new Reference($eventDecorator);
        }, $eventDecorators);

        $container->findDefinition(EventDecoratorChain::class)
            ->setArguments($eventDecoratorDefinitions);
    }
}
