<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\Infrastructure\Controller;

use Symfony\Component\HttpFoundation\Response;
use Zisato\ApiBundle\Infrastructure\Service\ResponseServiceInterface;
use Zisato\CQRS\WriteModel\Service\CommandBus;

abstract class AbstractCommandAPIController implements CommandAPIController
{
    private CommandBus $commandBus;

    private ResponseServiceInterface $responseService;

    public function __construct(CommandBus $commandBus, ResponseServiceInterface $responseService)
    {
        $this->commandBus = $commandBus;
        $this->responseService = $responseService;
    }

    public function commandBus(): CommandBus
    {
        return $this->commandBus;
    }

    public function respondCreated(): Response
    {
        return $this->responseService->respondCreated();
    }

    public function respondUpdated(): Response
    {
        return $this->responseService->respondUpdated();
    }

    public function respondDeleted(): Response
    {
        return $this->responseService->respondDeleted();
    }
}
