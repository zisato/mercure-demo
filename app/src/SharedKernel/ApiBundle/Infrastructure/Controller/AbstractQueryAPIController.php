<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\Infrastructure\Controller;

use Symfony\Component\HttpFoundation\Response;
use Zisato\ApiBundle\Infrastructure\Service\ResponseServiceInterface;
use Zisato\CQRS\ReadModel\Service\QueryBus;

abstract class AbstractQueryAPIController implements QueryAPIController
{
    private QueryBus $queryBus;

    private ResponseServiceInterface $responseService;

    public function __construct(QueryBus $queryBus, ResponseServiceInterface $responseService)
    {
        $this->queryBus = $queryBus;
        $this->responseService = $responseService;
    }

    public function queryBus(): QueryBus
    {
        return $this->queryBus;
    }

    public function respondSuccess(?array $data = null): Response
    {
        return $this->responseService->respondSuccess($data);
    }

    public function respondCollection(
        array $data,
        int $totalItems,
        int $page,
        int $perPage,
        int $totalPages
    ): Response {
        return $this->responseService->respondCollection($data, $totalItems, $page, $perPage, $totalPages);
    }
}
