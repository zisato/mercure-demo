<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\Infrastructure\Controller;

use Symfony\Component\HttpFoundation\Response;
use Zisato\CQRS\WriteModel\Service\CommandBus;

interface CommandAPIController
{
    public function commandBus(): CommandBus;

    public function respondCreated(): Response;

    public function respondUpdated(): Response;

    public function respondDeleted(): Response;
}
