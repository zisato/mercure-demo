<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\Infrastructure\Controller;

use Symfony\Component\HttpFoundation\Response;
use Zisato\CQRS\ReadModel\Service\QueryBus;

interface QueryAPIController
{
    public function queryBus(): QueryBus;

    /**
     * @param array<string|int, mixed>|null $data
     */
    public function respondSuccess(?array $data = null): Response;

    /**
     * @param array<string|int, mixed> $data
     */
    public function respondCollection(
        array $data,
        int $totalItems,
        int $page,
        int $perPage,
        int $totalPages
    ): Response;
}
