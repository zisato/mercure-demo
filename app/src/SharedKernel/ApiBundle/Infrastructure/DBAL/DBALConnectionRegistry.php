<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\Infrastructure\DBAL;

use Doctrine\DBAL\Connection;
use Doctrine\Persistence\ConnectionRegistry;

class DBALConnectionRegistry implements ConnectionRegistry
{
    private const DEFAULT_NAME = 'default_connection';

    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getDefaultConnectionName(): string
    {
        return self::DEFAULT_NAME;
    }

    public function getConnection($name = null): Connection
    {
        return $this->connection;
    }

    public function getConnections(): array
    {
        return [self::DEFAULT_NAME => $this->connection];
    }

    public function getConnectionNames(): array
    {
        return [self::DEFAULT_NAME];
    }
}
