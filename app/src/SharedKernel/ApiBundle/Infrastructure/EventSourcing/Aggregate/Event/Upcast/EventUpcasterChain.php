<?php

namespace Zisato\ApiBundle\Infrastructure\EventSourcing\Aggregate\Event\Upcast;

use Zisato\EventSourcing\Aggregate\Event\EventInterface;
use Zisato\EventSourcing\Aggregate\Event\Upcast\UpcasterInterface;

class EventUpcasterChain implements UpcasterInterface
{
    private EventUpcasterGroupCollection $eventUpcasterGroupCollection;

    public function __construct(EventUpcasterGroupCollection $eventUpcasterGroupCollection)
    {
        $this->eventUpcasterGroupCollection = $eventUpcasterGroupCollection;
    }

    public function canUpcast(EventInterface $event): bool
    {
        return $this->eventUpcasterGroupCollection->exists(\get_class($event));
    }

    public function upcast(EventInterface $event): EventInterface
    {
        $upcasters = $this->eventUpcasterGroupCollection->get(\get_class($event)) ?? [];

        foreach ($upcasters as $upcaster) {
            if ($upcaster->canUpcast($event)) {
                $event = $upcaster->upcast($event);
            }
        }

        return $event;
    }
}
