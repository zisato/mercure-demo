<?php

namespace Zisato\ApiBundle\Infrastructure\EventSourcing\Aggregate\Event\Upcast;

use Zisato\EventSourcing\Aggregate\Event\Upcast\UpcasterInterface;

class EventUpcasterGroup
{
    private string $name;
    /**
     * @var array<UpcasterInterface> $upcasters
     */
    private array $upcasters = [];

    public function __construct(string $name, UpcasterInterface ...$upcasters)
    {
        $this->name = $name;
        $this->upcasters = $upcasters;
    }

    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return array<UpcasterInterface> $upcasters
     */
    public function upcasters(): array
    {
        return $this->upcasters;
    }
}
