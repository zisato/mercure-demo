<?php

namespace Zisato\ApiBundle\Infrastructure\EventSourcing\Aggregate\Event\Upcast;

use Zisato\EventSourcing\Aggregate\Event\Upcast\UpcasterInterface;

class EventUpcasterGroupCollection
{
    /**
     * @var array<string, array<EventUpcasterGroup>> $eventUpcasterGroups
     */
    private array $eventUpcasterGroups = [];

    public function __construct(EventUpcasterGroup ...$eventUpcasterGroups)
    {
        foreach ($eventUpcasterGroups as $eventUpcasterGroup) {
            $this->eventUpcasterGroups[$eventUpcasterGroup->name()] = $eventUpcasterGroup->upcasters();
        }
    }

    public function exists(string $name): bool
    {
        return isset($this->eventUpcasterGroups[$name]);
    }

    /**
     * @return null|array<UpcasterInterface> $upcasters
     */
    public function get(string $name): ?array
    {
        return $this->eventUpcasterGroups[$name] ?? null;
    }
}
