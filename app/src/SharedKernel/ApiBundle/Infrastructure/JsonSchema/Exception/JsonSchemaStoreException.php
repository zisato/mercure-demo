<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\Infrastructure\JsonSchema\Exception;

class JsonSchemaStoreException extends \Exception
{
}
