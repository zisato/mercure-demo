<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\Infrastructure\JsonSchema\Store;

use Zisato\ApiBundle\Infrastructure\JsonSchema\Exception\JsonSchemaStoreException;

class LocalJsonSchemaStore implements JsonSchemaStoreInterface
{
    private string $jsonSchemaPath;

    public function __construct(string $jsonSchemaPath)
    {
        $this->jsonSchemaPath = $jsonSchemaPath;
    }

    public function get(string $schemaName): string
    {
        $filename = $this->jsonSchemaPath . $schemaName;

        if (! \is_file($filename)) {
            throw new JsonSchemaStoreException(\sprintf('File %s not exists', $filename));
        }

        $content = @\file_get_contents($filename);

        if ($content === false) {
            $error = \error_get_last();

            throw new JsonSchemaStoreException(\sprintf(
                'Could not get file content of %s. Error: %s',
                $filename,
                $error['message']
            ));
        }

        return $content;
    }
}
