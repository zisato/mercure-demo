<?php

namespace Zisato\ApiBundle\Infrastructure\Opis\JsonSchema;

use JsonException;
use Opis\JsonSchema\Validator;
use Zisato\ApiBundle\Infrastructure\JsonSchema\Exception\JsonSchemaValidatorException;
use Zisato\ApiBundle\Infrastructure\JsonSchema\Validator\JsonSchemaValidatorInterface;
use Zisato\ApiBundle\JSON\JSON;

class JsonSchemaValidator implements JsonSchemaValidatorInterface
{
    private Validator $validator;

    public function __construct(Validator $validator)
    {
        $this->validator = $validator;
    }

    public function validateData(string $json, string $jsonSchema): array
    {
        try {
            $data = JSON::decode($json, true);
        } catch (JsonException $exception) {
            throw new JsonSchemaValidatorException(\sprintf('Invalid json. Error: %s', $exception->getMessage()));
        }

        $result = $this->validator->validate($data, $jsonSchema);

        if ($result->hasError()) {
            throw new JsonSchemaValidatorException(\sprintf(
                'Invalid json schema. Error: %s',
                $result->error()->keyword()
            ));
        }

        return $data;
    }
}
