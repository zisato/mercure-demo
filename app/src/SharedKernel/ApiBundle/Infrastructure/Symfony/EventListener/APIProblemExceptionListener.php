<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\Infrastructure\Symfony\EventListener;

use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Zisato\ApiBundle\Infrastructure\ExceptionHandler\ExceptionHandlerServiceInterface;

class APIProblemExceptionListener
{
    private ExceptionHandlerServiceInterface $exceptionHandlerService;

    public function __construct(ExceptionHandlerServiceInterface $exceptionHandlerService)
    {
        $this->exceptionHandlerService = $exceptionHandlerService;
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        var_dump($event->getThrowable()->getMessage());
        $event->setResponse($this->exceptionHandlerService->handle($event->getThrowable()));
    }
}
