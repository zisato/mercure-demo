<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\Infrastructure\Symfony\ExceptionHandler;

use Symfony\Component\HttpFoundation\Response;
use Throwable;
use Zisato\ApiBundle\Infrastructure\ExceptionHandler\ExceptionHandlerServiceInterface;
use Zisato\ApiBundle\Infrastructure\ExceptionHandler\Strategy\ExceptionHandlerStrategyInterface;
use Zisato\ApiBundle\Infrastructure\Service\ResponseServiceInterface;

class ExceptionHandlerService implements ExceptionHandlerServiceInterface
{
    private ResponseServiceInterface $responseService;

    private ExceptionHandlerStrategyInterface $exceptionHandlerStrategy;

    public function __construct(
        ResponseServiceInterface $responseService,
        ExceptionHandlerStrategyInterface $exceptionHandlerStrategy
    ) {
        $this->responseService = $responseService;
        $this->exceptionHandlerStrategy = $exceptionHandlerStrategy;
    }

    public function handle(Throwable $exception): Response
    {
        if ($this->exceptionHandlerStrategy->canHandle($exception)) {
            return $this->exceptionHandlerStrategy->handle($exception);
        }

        return $this->responseService->respondError();
    }
}
