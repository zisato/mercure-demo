<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\Infrastructure\Symfony\Service;

use InvalidArgumentException;
use Symfony\Component\HttpFoundation\RequestStack;
use Zisato\ApiBundle\Infrastructure\JsonSchema\Store\JsonSchemaStoreInterface;
use Zisato\ApiBundle\Infrastructure\JsonSchema\Validator\JsonSchemaValidatorInterface;
use Zisato\ApiBundle\Infrastructure\Service\RequestBodyServiceInterface;

class RequestBodyService implements RequestBodyServiceInterface
{
    private RequestStack $requestStack;

    private JsonSchemaStoreInterface $jsonSchemaStore;

    private JsonSchemaValidatorInterface $jsonSchemaValidator;

    public function __construct(
        RequestStack $requestStack,
        JsonSchemaStoreInterface $jsonSchemaStore,
        JsonSchemaValidatorInterface $jsonSchemaValidator
    ) {
        $this->requestStack = $requestStack;
        $this->jsonSchemaStore = $jsonSchemaStore;
        $this->jsonSchemaValidator = $jsonSchemaValidator;
    }

    /**
     * @return array<string, mixed>
     */
    public function requestBody(string $schemaName): array
    {
        $request = $this->requestStack->getCurrentRequest();

        if ($request === null) {
            throw new InvalidArgumentException('Request cannot be null.');
        }

        $jsonSchema = $this->jsonSchemaStore->get($schemaName);

        /** @var string $requestContent */
        $requestContent = $request->getContent();

        return $this->jsonSchemaValidator->validateData($requestContent, $jsonSchema);
    }
}
