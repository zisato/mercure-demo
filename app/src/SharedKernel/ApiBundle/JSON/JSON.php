<?php

namespace Zisato\ApiBundle\JSON;

class JSON
{
    private const JSON_DEPTH = 512;

    /**
     * @param \stdClass|array $json
     */
    public static function encode($json): string
    {
        return \json_encode(
            $json,
            JSON_THROW_ON_ERROR,
            self::JSON_DEPTH
        );
    }

    /**
     * @return \stdClass|array
     */
    public static function decode(string $json, bool $associative = false)
    {
        return \json_decode(
            $json,
            $associative,
            self::JSON_DEPTH,
            JSON_THROW_ON_ERROR
        );
    }
}
