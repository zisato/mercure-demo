<?php

declare(strict_types=1);

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

use Doctrine\DBAL\Connection;
use Symfony\Component\Messenger\Transport\Doctrine\DoctrineTransportFactory;
use Zisato\ApiBundle\DependencyInjection\ApiExtension;
use Zisato\ApiBundle\Infrastructure\DBAL\DBALConnectionRegistry;
use Zisato\ApiBundle\Infrastructure\Symfony\MessageHandler\Bus\MessengerCommandBus;
use Zisato\ApiBundle\Infrastructure\Symfony\MessageHandler\Bus\MessengerEventBus;
use Zisato\ApiBundle\Infrastructure\Symfony\MessageHandler\Bus\MessengerQueryBus;
use Zisato\ApiBundle\Infrastructure\Symfony\MessageHandler\Middleware\DBALTransactionMiddleware;
use Zisato\CQRS\ReadModel\Service\QueryBus;
use Zisato\CQRS\WriteModel\Service\CommandBus;
use Zisato\EventSourcing\Aggregate\Event\Bus\EventBusInterface;

return static function (ContainerConfigurator $container) {
    $container->services()

        ->set(DBALConnectionRegistry::class, DBALConnectionRegistry::class)
        ->args([service(Connection::class)])

        ->set(DoctrineTransportFactory::class, DoctrineTransportFactory::class)
        ->args([service(DBALConnectionRegistry::class)])

        ->set(MessengerEventBus::class, MessengerEventBus::class)
        ->args([service(ApiExtension::BUS_EVENTS)])
        ->alias(EventBusInterface::class, MessengerEventBus::class)

        ->set(MessengerCommandBus::class, MessengerCommandBus::class)
        ->args([service(ApiExtension::BUS_COMMANDS)])
        ->alias(CommandBus::class, MessengerCommandBus::class)

        ->set(MessengerQueryBus::class, MessengerQueryBus::class)
        ->args([service(ApiExtension::BUS_QUERIES)])
        ->alias(QueryBus::class, MessengerQueryBus::class)

        ->set(DBALTransactionMiddleware::class, DBALTransactionMiddleware::class)
        ->args([service(Connection::class)])
    ;
};
