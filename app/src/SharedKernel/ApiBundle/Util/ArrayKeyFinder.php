<?php

declare(strict_types=1);

namespace Zisato\ApiBundle\Util;

trait ArrayKeyFinder
{
    /**
     * @param array<int|string, mixed> $array
     * @return int|string
     */
    public function index(array $array, callable $callback)
    {
        $key = null;

        foreach ($array as $index => $data) {
            if ($callback($data)) {
                $key = $index;

                break;
            }
        }

        if ($key === null) {
            throw new \InvalidArgumentException('Index not found');
        }

        return $key;
    }
}
