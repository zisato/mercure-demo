<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Event\Serializer;

use DateTimeImmutable;
use Zisato\EventSourcing\Aggregate\Event\EventInterface;
use Zisato\EventSourcing\Aggregate\Event\Version\VersionResolverInterface;
use Zisato\EventSourcing\JSON\JSON;

class EventSerializer implements EventSerializerInterface
{
    private VersionResolverInterface $versionResolver;

    public function __construct(VersionResolverInterface $versionResolver)
    {
        $this->versionResolver = $versionResolver;
    }

    public function fromArray(array $data): EventInterface
    {
        /** @var callable $callable */
        $callable = [$data['event_class'], 'reconstitute'];

        return \call_user_func(
            $callable,
            $data['aggregate_id'],
            (int) $data['aggregate_version'],
            new DateTimeImmutable($data['created_at']),
            JSON::decode($data['payload']),
            (int) $data['version'],
            JSON::decode($data['metadata']),
        );
    }

    public function toArray(EventInterface $event): array
    {
        return [
            'event_class' => \get_class($event),
            'aggregate_id' => $event->aggregateId(),
            'aggregate_version' => $event->aggregateVersion(),
            'created_at' => $event->createdAt()
                ->format(static::DATE_FORMAT),
            'payload' => JSON::encode($event->payload()),
            'version' => $this->versionResolver->resolve($event),
            'metadata' => JSON::encode($event->metadata()),
        ];
    }
}
