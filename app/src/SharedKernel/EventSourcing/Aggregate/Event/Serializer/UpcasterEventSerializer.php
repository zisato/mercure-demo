<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Event\Serializer;

use Zisato\EventSourcing\Aggregate\Event\EventInterface;
use Zisato\EventSourcing\Aggregate\Event\Upcast\UpcasterInterface;

class UpcasterEventSerializer implements EventSerializerInterface
{
    private EventSerializerInterface $eventSerializer;

    private UpcasterInterface $upcaster;

    public function __construct(EventSerializerInterface $eventSerializer, UpcasterInterface $upcaster)
    {
        $this->eventSerializer = $eventSerializer;
        $this->upcaster = $upcaster;
    }

    public function fromArray(array $data): EventInterface
    {
        $event = $this->eventSerializer->fromArray($data);

        if ($this->upcaster->canUpcast($event)) {
            $event = $this->upcaster->upcast($event);
        }

        return $event;
    }

    public function toArray(EventInterface $event): array
    {
        return $this->eventSerializer->toArray($event);
    }
}
