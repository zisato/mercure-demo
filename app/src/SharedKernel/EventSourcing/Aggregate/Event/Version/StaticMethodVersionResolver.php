<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Event\Version;

use Zisato\EventSourcing\Aggregate\Event\EventInterface;

class StaticMethodVersionResolver implements VersionResolverInterface
{
    private const METHOD_NAME = 'defaultVersion';

    private string $method;

    public function __construct(string $method = self::METHOD_NAME)
    {
        $this->method = $method;
    }

    public function resolve(EventInterface $event): int
    {
        if (\method_exists($event, $this->method)) {
            /** @var callable $callable */
            $callable = [$event, $this->method];

            return \call_user_func($callable);
        }

        return $event->version();
    }
}
