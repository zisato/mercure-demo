<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Exception;

class AggregateReconstituteException extends \RuntimeException
{
}
