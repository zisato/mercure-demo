<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\ValueObject;

class Version
{
    private const VALUE_MIN = 0;

    private int $value;

    final private function __construct(int $value)
    {
        $this->checkValidValue($value);

        $this->value = $value;
    }

    public static function create(int $value): self
    {
        return new static($value);
    }

    public static function zero(): self
    {
        return new static(self::VALUE_MIN);
    }

    public function value(): int
    {
        return $this->value;
    }

    public function next(): self
    {
        return new static($this->value + 1);
    }

    public function equals(self $version): bool
    {
        return $this->value() === $version->value();
    }

    private function checkValidValue(int $value): void
    {
        if ($value < self::VALUE_MIN) {
            throw new \InvalidArgumentException(\sprintf(
                'Invalid Version value. Min allowed: %d',
                self::VALUE_MIN
            ));
        }
    }
}
