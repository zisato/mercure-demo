<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Event;

class Event implements EventInterface
{
    private \DateTimeImmutable $createdAt;

    /**
     * @var array<string, mixed>
     */
    private array $payload;

    /**
     * @param array<string, mixed> $payload
     */
    public function __construct(\DateTimeImmutable $createdAt, array $payload)
    {
        $this->createdAt = $createdAt;
        $this->payload = $payload;
    }

    public function createdAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function payload(): array
    {
        return $this->payload;
    }
}
