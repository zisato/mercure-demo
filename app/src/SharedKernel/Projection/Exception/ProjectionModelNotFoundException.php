<?php

declare(strict_types=1);

namespace Zisato\Projection\Exception;

class ProjectionModelNotFoundException extends \Exception
{
}
