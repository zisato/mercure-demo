<?php

namespace RetroBoard\Tests\Functional\Context;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Zisato\CQRS\WriteModel\Service\CommandBus;
use RetroBoard\Application\Board\Command\CreateBoard\CreateBoardCommand;

class BoardContext implements Context
{
    private static CommandBus $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        self::$commandBus = $commandBus;
    }

    /**
     * @Given /^the following boards exists:$/
     *
     * @param TableNode $table
     */
    public function theFollowingBoardsExists(TableNode $table): void
    {
        foreach ($table as $row) {
            $backgroundColor = null;
            if ($row['background_color'] && !empty(trim($row['background_color']))) {
                $backgroundColor = $row['background_color'];
            }

            self::$commandBus->handle(new CreateBoardCommand(
                $row['id'],
                $row['name'],
                $row['meeting_id'],
                $backgroundColor
            ));
        }
    }
}
