<?php

namespace RetroBoard\Tests\Functional\Context;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Zisato\CQRS\WriteModel\Service\CommandBus;
use RetroBoard\Application\Card\Command\CreateCard\CreateCardCommand;

class CardContext implements Context
{
    private static CommandBus $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        self::$commandBus = $commandBus;
    }

    /**
     * @Given /^the following cards exists:$/
     *
     * @param TableNode $table
     */
    public function theFollowingCardsExists(TableNode $table): void
    {
        foreach ($table as $row) {
            self::$commandBus->handle(new CreateCardCommand(
                $row['id'],
                $row['text'],
                $row['board_id']
            ));
        }
    }
}
