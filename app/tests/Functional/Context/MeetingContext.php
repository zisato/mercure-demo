<?php

namespace RetroBoard\Tests\Functional\Context;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Zisato\CQRS\WriteModel\Service\CommandBus;
use RetroBoard\Application\Meeting\Command\CreateMeeting\CreateMeetingCommand;
use RetroBoard\Tests\Functional\Context\Wildcards\WildcardsTrait;

class MeetingContext implements Context
{
    use WildcardsTrait {
        WildcardsTrait::__construct as private __wilcardsConstruct;
    }

    private static CommandBus $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->__wilcardsConstruct();
        
        self::$commandBus = $commandBus;
    }

    /**
     * @Given /^the following meetings exists:$/
     *
     * @param TableNode $table
     */
    public function theFollowingSprintsExists(TableNode $table): void
    {
        foreach ($table as $row) {
            $name = (string) $this->replaceWildcards($row['name']);

            self::$commandBus->handle(new CreateMeetingCommand(
                $row['id'],
                $name
            ));
        }
    }
}
