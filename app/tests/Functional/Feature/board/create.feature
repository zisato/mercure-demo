Feature:
  In order to use RetroBoard
  I want to create boards

  Background: Creates meeting
    Given the following meetings exists:
    | id                                   | name |
    | 09eb1b50-1e9f-11ea-978f-2e728ce88125 | 2012 |

  Scenario: It creates a board with required parameters
    When I call "POST" "/api/boards" with body:
    """
      {
        "data": {
          "id": "0ae12040-4b00-11e9-b475-0800200c9a66",
          "attributes": {
            "name": "KEEP"
          },
          "relationships": {
            "meeting": {
              "id": "09eb1b50-1e9f-11ea-978f-2e728ce88125"
            }
          }
        }
      }
    """
    Then the status code should be 204
    And the response should be empty
    Then I call "GET" "/api/meetings/09eb1b50-1e9f-11ea-978f-2e728ce88125"
    Then the status code should be 200
    And the response should matches
    """
      {
        "data":{
            "id":"09eb1b50-1e9f-11ea-978f-2e728ce88125",
            "attributes":{
              "name":"2012"
            },
            "relationships":{
              "boards":[
                  {
                    "id":"0ae12040-4b00-11e9-b475-0800200c9a66",
                    "attributes":{
                        "name":"KEEP",
                        "background_color":null
                    },
                    "relationships":{
                        "meeting":{
                          "id":"09eb1b50-1e9f-11ea-978f-2e728ce88125"
                        },
                        "cards":[]
                    }
                  }
              ]
            }
        }
      }
    """

  Scenario: It creates a board with all parameters
    When I call "POST" "/api/boards" with body:
    """
      {
        "data": {
          "id": "0ae12040-4b00-11e9-b475-0800200c9a66",
          "attributes": {
            "name": "KEEP",
            "background_color": "bg-info"
          },
          "relationships": {
            "meeting": {
              "id": "09eb1b50-1e9f-11ea-978f-2e728ce88125"
            }
          }
        }
      }
    """
    Then the status code should be 204
    And the response should be empty
    Then I call "GET" "/api/meetings/09eb1b50-1e9f-11ea-978f-2e728ce88125"
    Then the status code should be 200
    And the response should matches
    """
      {
        "data":{
            "id":"09eb1b50-1e9f-11ea-978f-2e728ce88125",
            "attributes":{
              "name":"2012"
            },
            "relationships":{
              "boards":[
                  {
                    "id":"0ae12040-4b00-11e9-b475-0800200c9a66",
                    "attributes":{
                        "name":"KEEP",
                        "background_color":"bg-info"
                    },
                    "relationships":{
                        "meeting":{
                          "id":"09eb1b50-1e9f-11ea-978f-2e728ce88125"
                        },
                        "cards":[]
                    }
                  }
              ]
            }
        }
      }
    """

  Scenario: It returns error on create with existing id
    When I call "POST" "/api/boards" with body:
    """
      {
        "data": {
          "id": "0ae12040-4b00-11e9-b475-0800200c9a66",
          "attributes": {
            "name": "KEEP"
          },
          "relationships": {
            "meeting": {
              "id": "09eb1b50-1e9f-11ea-978f-2e728ce88125"
            }
          }
        }
      }
    """
    Then the status code should be 204
    And the response should be empty
    Then I call "POST" "/api/boards" with body:
    """
      {
        "data": {
          "id": "0ae12040-4b00-11e9-b475-0800200c9a66",
          "attributes": {
            "name": "KEEP"
          },
          "relationships": {
            "meeting": {
              "id": "09eb1b50-1e9f-11ea-978f-2e728ce88125"
            }
          }
        }
      }
    """
    Then the status code should be 400
    And the response should be a JSON like
    """
      {
        "status": 400,
        "type": "about:blank",
        "title": "Bad Request"
      }
    """

  Scenario: It returns error on create when sprint not exists
    When I call "POST" "/api/boards" with body:
    """
      {
        "data": {
          "id": "0ae12040-4b00-11e9-b475-0800200c9a66",
          "attributes": {
            "name": "KEEP"
          },
          "relationships": {
            "meeting": {
              "id": "7af68708-f85b-11ea-adc1-0242ac120002"
            }
          }
        }
      }
    """
    Then the status code should be 404
    And the response should be a JSON like
    """
      {
        "status": 404,
        "type": "about:blank",
        "title": "Not Found"
      }
    """
