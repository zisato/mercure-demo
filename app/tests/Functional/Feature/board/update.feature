Feature:
  In order to use RetroBoard
  I want to update boards

  Background: Creates meeting and board
    Given the following meetings exists:
    | id                                   | name |
    | 3d900d32-8d47-11ea-bc55-0242ac130003 | 2012 |
    | 09eb1b50-1e9f-11ea-978f-2e728ce88125 | 2012 |
    Given the following boards exists:
    | id                                   | name  | meeting_id                           | background_color |
    | 0ae12040-4b00-11e9-b475-0800200c9a66 | KEEP  | 3d900d32-8d47-11ea-bc55-0242ac130003 | bg-info          |

  Scenario: It updates board name
    When I call "PATCH" "/api/boards/0ae12040-4b00-11e9-b475-0800200c9a66" with body:
    """
      {
        "data": {
          "attributes": {
            "name": "New name"
          }
        }
      }
    """
    Then the status code should be 204
    And the response should be empty
    Then I call "GET" "/api/meetings/3d900d32-8d47-11ea-bc55-0242ac130003"
    And the property "data.relationships.boards.0.attributes.name" should be equals "New name"

  Scenario: It updates board background color
    When I call "PATCH" "/api/boards/0ae12040-4b00-11e9-b475-0800200c9a66" with body:
    """
      {
        "data": {
          "attributes": {
            "background_color": "bg-danger"
          }
        }
      }
    """
    Then the status code should be 204
    And the response should be empty
    Then I call "GET" "/api/meetings/3d900d32-8d47-11ea-bc55-0242ac130003"
    And the property "data.relationships.boards.0.attributes.background_color" should be equals "bg-danger"

  Scenario: It updates board background color to null
    When I call "PATCH" "/api/boards/0ae12040-4b00-11e9-b475-0800200c9a66" with body:
    """
      {
        "data": {
          "attributes": {
            "background_color": null
          }
        }
      }
    """
    Then the status code should be 204
    And the response should be empty
    Then I call "GET" "/api/meetings/3d900d32-8d47-11ea-bc55-0242ac130003"
    And the property "data.relationships.boards.0.attributes.background_color" should be equals "null"
