Feature:
  In order to use RetroBoard
  I want to create cards

  Background: Creates user, sprint and board
    Given the following meetings exists:
    | id                                   | name |
    | 09eb1b50-1e9f-11ea-978f-2e728ce88125 | 2022 |
    Given the following boards exists:
    | id                                   | name  | meeting_id                         | background_color |
    | 0ae12040-4b00-11e9-b475-0800200c9a66 | KEEP  | 09eb1b50-1e9f-11ea-978f-2e728ce88125 | bg-info          |

  Scenario: It creates a card with required parameters
    When I call "POST" "/api/cards" with body:
    """
      {
        "data": {
          "id": "d20dc206-ed97-11e9-81b4-2a2ae2dbcce4",
          "attributes": {
            "text": "My awesome text"
          },
          "relationships": {
            "board": {
              "id": "0ae12040-4b00-11e9-b475-0800200c9a66"
            }
          }
        }
      }
    """
    Then the status code should be 204
    And the response should be empty
    Then I call "GET" "/api/meetings/09eb1b50-1e9f-11ea-978f-2e728ce88125"
    Then the status code should be 200
    And the response should matches
    """
      {
        "data":{
          "id":"09eb1b50-1e9f-11ea-978f-2e728ce88125",
          "attributes":{
            "name":"2022"
          },
          "relationships":{
            "boards":[
              {
                "id":"0ae12040-4b00-11e9-b475-0800200c9a66",
                "attributes":{
                    "name":"KEEP",
                    "background_color":"bg-info"
                },
                "relationships":{
                  "meeting":{
                    "id":"09eb1b50-1e9f-11ea-978f-2e728ce88125"
                  },
                  "cards": [
                    {
                      "id": "d20dc206-ed97-11e9-81b4-2a2ae2dbcce4",
                      "attributes": {
                        "text": "My awesome text"
                      },
                      "relationships": {
                        "board": {
                          "id": "0ae12040-4b00-11e9-b475-0800200c9a66"
                        }
                      }
                    }
                  ]
                }
              }
            ]
          }
        }
      }
    """

  Scenario: It returns error on create with existing id
    When I call "POST" "/api/cards" with body:
    """
      {
        "data": {
          "id": "d20dc206-ed97-11e9-81b4-2a2ae2dbcce4",
          "attributes": {
            "text": "My awesome text"
          },
          "relationships": {
            "board": {
              "id": "0ae12040-4b00-11e9-b475-0800200c9a66"
            }
          }
        }
      }
    """
    Then the status code should be 204
    And the response should be empty
    Then I call "POST" "/api/cards" with body:
    """
      {
        "data": {
          "id": "d20dc206-ed97-11e9-81b4-2a2ae2dbcce4",
          "attributes": {
            "text": "My awesome text"
          },
          "relationships": {
            "board": {
              "id": "0ae12040-4b00-11e9-b475-0800200c9a66"
            }
          }
        }
      }
    """
    Then the status code should be 400
    And the response should be a JSON like
    """
      {
        "status": 400,
        "type": "about:blank",
        "title": "Bad Request"
      }
    """

  Scenario: It returns error on create when not exists board id
    When I call "POST" "/api/cards" with body:
    """
      {
        "data": {
          "id": "d20dc206-ed97-11e9-81b4-2a2ae2dbcce4",
          "attributes": {
            "text": "My awesome text"
          },
          "relationships": {
            "board": {
              "id": "d4b7e8d4-edd2-11e9-81b4-2a2ae2dbcce4"
            }
          }
        }
      }
    """
    Then the status code should be 404
    And the response should be a JSON like
    """
      {
        "status": 404,
        "type": "about:blank",
        "title": "Not Found"
      }
    """
