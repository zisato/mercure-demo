Feature:
  In order to use RetroBoard
  I want to get cards

  Background: Creates user, sprint, board and card
    Given the following meetings exists:
    | id                                   | name |
    | 09eb1b50-1e9f-11ea-978f-2e728ce88125 | 2022 |
    Given the following boards exists:
    | id                                   | name  | meeting_id                         | background_color |
    | 0ae12040-4b00-11e9-b475-0800200c9a66 | KEEP  | 09eb1b50-1e9f-11ea-978f-2e728ce88125 | bg-info          |
    Given the following cards exists:
    | id                                   | text            | user_id                              | board_id                             |
    | d20dc206-ed97-11e9-81b4-2a2ae2dbcce4 | My awesome text | 96714496-fd6b-11e9-9200-0242ac120005 | 0ae12040-4b00-11e9-b475-0800200c9a66 |

  Scenario: It returns detail successfully
    Then I call "GET" "/api/meetings/09eb1b50-1e9f-11ea-978f-2e728ce88125"
    Then the status code should be 200
    And the response should matches
    """
      {
        "data":{
          "id":"09eb1b50-1e9f-11ea-978f-2e728ce88125",
          "attributes":{
            "name":"2022"
          },
          "relationships":{
            "boards":[
              {
                "id":"0ae12040-4b00-11e9-b475-0800200c9a66",
                "attributes":{
                    "name":"KEEP",
                    "background_color":"bg-info"
                },
                "relationships":{
                  "meeting":{
                    "id":"09eb1b50-1e9f-11ea-978f-2e728ce88125"
                  },
                  "cards": [
                    {
                      "id": "d20dc206-ed97-11e9-81b4-2a2ae2dbcce4",
                      "attributes": {
                        "text": "My awesome text"
                      },
                      "relationships": {
                        "board": {
                          "id": "0ae12040-4b00-11e9-b475-0800200c9a66"
                        }
                      }
                    }
                  ]
                }
              }
            ]
          }
        }
      }
    """
