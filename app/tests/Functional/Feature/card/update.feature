Feature:
  In order to use RetroBoard
  I want to update cards

  Background: Creates user, sprint, board and card
    Given the following meetings exists:
    | id                                   | name |
    | 09eb1b50-1e9f-11ea-978f-2e728ce88125 | 2022 |
    Given the following boards exists:
    | id                                   | name  | meeting_id                         | background_color |
    | 0ae12040-4b00-11e9-b475-0800200c9a66 | KEEP  | 09eb1b50-1e9f-11ea-978f-2e728ce88125 | bg-info          |
    | 974becbe-e780-11e9-81b4-2a2ae2dbcce4 | START | 09eb1b50-1e9f-11ea-978f-2e728ce88125 |                  |
    Given the following cards exists:
    | id                                   | text            | user_id                              | board_id                             |
    | d20dc206-ed97-11e9-81b4-2a2ae2dbcce4 | My awesome text | 96714496-fd6b-11e9-9200-0242ac120005 | 0ae12040-4b00-11e9-b475-0800200c9a66 |

  Scenario: It updates a card text successfully
    When I call "PATCH" "/api/cards/d20dc206-ed97-11e9-81b4-2a2ae2dbcce4" with body:
    """
      {
        "data": {
          "attributes": {
            "text": "My new awesome text"
          }
        }
      }
    """
    Then the status code should be 204
    And the response should be empty
    Then I call "GET" "/api/meetings/09eb1b50-1e9f-11ea-978f-2e728ce88125"
    And the property "data.relationships.boards.0.relationships.cards.0.attributes.text" should be equals "My new awesome text"
