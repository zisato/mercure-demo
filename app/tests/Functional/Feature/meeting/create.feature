Feature:
  In order to use RetroBoard
  I want to create meetings

  Scenario: It creates an meeting with required parameters
    When I call "POST" "/api/meetings" with body:
    """
      {
        "data": {
          "id": "09eb1b50-1e9f-11ea-978f-2e728ce88125",
          "attributes": {
            "name": "MyMeetingName"
          }
        }
      }
    """
    Then the status code should be 204
    And the response should be empty

  Scenario: It returns error on create with existing id
    When I call "POST" "/api/meetings" with body:
    """
      {
        "data": {
          "id": "09eb1b50-1e9f-11ea-978f-2e728ce88125",
          "attributes": {
            "name": "MyMeetingName"
          }
        }
      }
    """
    Then the status code should be 204
    And the response should be empty
    Then I call "POST" "/api/meetings" with body:
    """
      {
        "data": {
          "id": "09eb1b50-1e9f-11ea-978f-2e728ce88125",
          "attributes": {
            "name": "MyMeetingName"
          }
        }
      }
    """
    Then the status code should be 400
    And the response should be a JSON like
    """
      {
        "status": 400,
        "type": "about:blank",
        "title": "Bad Request"
      }
    """
