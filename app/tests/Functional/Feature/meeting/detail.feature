Feature:
  In order to use RetroBoard
  I want to list meetings

  Background: Creates user, meeting, board and card
    Given the following meetings exists:
    | id                                   | name          |
    | 09eb1b50-1e9f-11ea-978f-2e728ce88125 | MyMeetingName |
    Given the following boards exists:
    | id                                   | name   | meeting_id                           | background_color |
    | 0ae12040-4b00-11e9-b475-0800200c9a66 | KEEP   | 09eb1b50-1e9f-11ea-978f-2e728ce88125 |                  |
    | 974becbe-e780-11e9-81b4-2a2ae2dbcce4 | START  | 09eb1b50-1e9f-11ea-978f-2e728ce88125 |                  |
    | a08fd33a-e780-11e9-81b4-2a2ae2dbcce4 | STOP   | 09eb1b50-1e9f-11ea-978f-2e728ce88125 |                  |
    | b0a10a82-e780-11e9-a359-2a2ae2dbcce4 | FLOWER | 09eb1b50-1e9f-11ea-978f-2e728ce88125 |                  |
    Given the following cards exists:
    | id                                   | text           | user_id                              | board_id                             |
    | d20dc206-ed97-11e9-81b4-2a2ae2dbcce4 | My keep text   | 96714496-fd6b-11e9-9200-0242ac120005 | 0ae12040-4b00-11e9-b475-0800200c9a66 |
    | 1c066fdc-b932-11ea-b3de-0242ac130004 | My start text  | 96714496-fd6b-11e9-9200-0242ac120005 | 974becbe-e780-11e9-81b4-2a2ae2dbcce4 |
    | 1fdd0bb6-b932-11ea-b3de-0242ac130004 | My stop text   | 96714496-fd6b-11e9-9200-0242ac120005 | a08fd33a-e780-11e9-81b4-2a2ae2dbcce4 |
    | 24806988-b932-11ea-b3de-0242ac130004 | My flower text | 96714496-fd6b-11e9-9200-0242ac120005 | b0a10a82-e780-11e9-a359-2a2ae2dbcce4 |

  Scenario: It returns detail successfully
    When I call "GET" "/api/meetings/09eb1b50-1e9f-11ea-978f-2e728ce88125"
    Then the status code should be 200
    And the response should matches
    """
      {
        "data": {
          "id":"09eb1b50-1e9f-11ea-978f-2e728ce88125",
          "attributes":{
            "name": "MyMeetingName"
          },
          "relationships":{
            "boards":[
              {
                "id":"0ae12040-4b00-11e9-b475-0800200c9a66",
                "attributes":{
                  "name":"KEEP",
                  "background_color":null
                },
                "relationships":{
                  "meeting":{
                    "id":"09eb1b50-1e9f-11ea-978f-2e728ce88125"
                  },
                  "cards":[
                    {	
                      "id":"d20dc206-ed97-11e9-81b4-2a2ae2dbcce4",	
                      "attributes":{	
                        "text":"My keep text"	
                      },
                      "relationships":{	
                        "board":{	
                          "id":"0ae12040-4b00-11e9-b475-0800200c9a66"	
                        }
                      }	
                    }
                  ]
                }
              },
              {
                "id":"974becbe-e780-11e9-81b4-2a2ae2dbcce4",
                "attributes":{
                  "name":"START",
                  "background_color":null
                },
                "relationships":{
                  "meeting":{
                    "id":"09eb1b50-1e9f-11ea-978f-2e728ce88125"
                  },
                  "cards":[
                    {	
                      "id":"1c066fdc-b932-11ea-b3de-0242ac130004",	
                      "attributes":{	
                        "text":"My start text"	
                      },
                      "relationships":{	
                        "board":{	
                          "id":"974becbe-e780-11e9-81b4-2a2ae2dbcce4"	
                        }
                      }	
                    }
                  ]
                }
              },
              {
                "id":"a08fd33a-e780-11e9-81b4-2a2ae2dbcce4",
                "attributes":{
                  "name":"STOP",
                  "background_color":null
                },
                "relationships":{
                  "meeting":{
                    "id":"09eb1b50-1e9f-11ea-978f-2e728ce88125"
                  },
                  "cards":[
                    {	
                      "id":"1fdd0bb6-b932-11ea-b3de-0242ac130004",	
                      "attributes":{	
                        "text":"My stop text"	
                      },
                      "relationships":{	
                        "board":{	
                          "id":"a08fd33a-e780-11e9-81b4-2a2ae2dbcce4"	
                        }
                      }	
                    }
                  ]
                }
              },
              {
                "id":"b0a10a82-e780-11e9-a359-2a2ae2dbcce4",
                "attributes":{
                  "name":"FLOWER",
                  "background_color":null
                },
                "relationships":{
                  "meeting":{
                    "id":"09eb1b50-1e9f-11ea-978f-2e728ce88125"
                  },
                  "cards":[
                    {	
                      "id":"24806988-b932-11ea-b3de-0242ac130004",	
                      "attributes":{	
                        "text":"My flower text"	
                      },
                      "relationships":{	
                        "board":{	
                          "id":"b0a10a82-e780-11e9-a359-2a2ae2dbcce4"	
                        }
                      }	
                    }
                  ]
                }
              }
            ]
          }
        }
      }
    """
