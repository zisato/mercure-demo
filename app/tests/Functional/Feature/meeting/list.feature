Feature:
  In order to use RetroBoard
  I want to list meetings

  Background: Creates user, meeting, board and card
    Given the following meetings exists:
    | id                                   | name |
    | 3d900d32-8d47-11ea-bc55-0242ac130003 | 2010 |
    | 09eb1b50-1e9f-11ea-978f-2e728ce88125 | 2014 |
    Given the following boards exists:
    | id                                   | name   | meeting_id                         | background_color |
    | 0ae12040-4b00-11e9-b475-0800200c9a66 | KEEP   | 3d900d32-8d47-11ea-bc55-0242ac130003 |                  |
    | 974becbe-e780-11e9-81b4-2a2ae2dbcce4 | START  | 3d900d32-8d47-11ea-bc55-0242ac130003 |                  |
    | a08fd33a-e780-11e9-81b4-2a2ae2dbcce4 | STOP   | 09eb1b50-1e9f-11ea-978f-2e728ce88125 |                  |
    | b0a10a82-e780-11e9-a359-2a2ae2dbcce4 | FLOWER | 09eb1b50-1e9f-11ea-978f-2e728ce88125 |                  |
    Given the following cards exists:
    | id                                   | text           | user_id                              | board_id                             |
    | d20dc206-ed97-11e9-81b4-2a2ae2dbcce4 | My keep text   | 96714496-fd6b-11e9-9200-0242ac120005 | 0ae12040-4b00-11e9-b475-0800200c9a66 |
    | 1c066fdc-b932-11ea-b3de-0242ac130004 | My start text  | 96714496-fd6b-11e9-9200-0242ac120005 | 974becbe-e780-11e9-81b4-2a2ae2dbcce4 |
    | 1fdd0bb6-b932-11ea-b3de-0242ac130004 | My stop text   | 96714496-fd6b-11e9-9200-0242ac120005 | a08fd33a-e780-11e9-81b4-2a2ae2dbcce4 |
    | 24806988-b932-11ea-b3de-0242ac130004 | My flower text | 96714496-fd6b-11e9-9200-0242ac120005 | b0a10a82-e780-11e9-a359-2a2ae2dbcce4 |

  Scenario: It list meetings successfully
    When I call "GET" "/api/meetings"
    Then the status code should be 200
    And the response should matches
    """
      {
        "data":[
          {
            "id":"3d900d32-8d47-11ea-bc55-0242ac130003",
            "attributes":{
              "name":"2010"
            },
            "relationships":{
              "boards":[
                {
                  "id":"0ae12040-4b00-11e9-b475-0800200c9a66",
                  "attributes":{
                    "name":"KEEP",
                    "background_color":null
                  },
                  "relationships":{
                    "meeting":{
                      "id":"3d900d32-8d47-11ea-bc55-0242ac130003"
                    },
                    "cards":[
                      {	
                        "id":"d20dc206-ed97-11e9-81b4-2a2ae2dbcce4",	
                        "attributes":{	
                          "text":"My keep text"	
                        },
                        "relationships":{	
                          "board":{	
                            "id":"0ae12040-4b00-11e9-b475-0800200c9a66"	
                          }
                        }	
                      }
                    ]
                  }
                },
                {
                  "id":"974becbe-e780-11e9-81b4-2a2ae2dbcce4",
                  "attributes":{
                    "name":"START",
                    "background_color":null
                  },
                  "relationships":{
                    "meeting":{
                      "id":"3d900d32-8d47-11ea-bc55-0242ac130003"
                    },
                    "cards":[
                      {	
                        "id":"1c066fdc-b932-11ea-b3de-0242ac130004",	
                        "attributes":{	
                          "text":"My start text"	
                        },
                        "relationships":{	
                          "board":{	
                            "id":"974becbe-e780-11e9-81b4-2a2ae2dbcce4"	
                          }
                        }	
                      }
                    ]
                  }
                }
              ]
            }
          },
          {
            "id":"09eb1b50-1e9f-11ea-978f-2e728ce88125",
            "attributes":{
              "name":"2014"
            },
            "relationships":{
              "boards":[
                {
                  "id":"a08fd33a-e780-11e9-81b4-2a2ae2dbcce4",
                  "attributes":{
                    "name":"STOP",
                    "background_color":null
                  },
                  "relationships":{
                    "meeting":{
                      "id":"09eb1b50-1e9f-11ea-978f-2e728ce88125"
                    },
                    "cards":[
                      {	
                        "id":"1fdd0bb6-b932-11ea-b3de-0242ac130004",	
                        "attributes":{	
                          "text":"My stop text"	
                        },
                        "relationships":{	
                          "board":{	
                            "id":"a08fd33a-e780-11e9-81b4-2a2ae2dbcce4"	
                          }
                        }	
                      }
                    ]
                  }
                },
                {
                  "id":"b0a10a82-e780-11e9-a359-2a2ae2dbcce4",
                  "attributes":{
                    "name":"FLOWER",
                    "background_color":null
                  },
                  "relationships":{
                    "meeting":{
                      "id":"09eb1b50-1e9f-11ea-978f-2e728ce88125"
                    },
                    "cards":[
                      {	
                        "id":"24806988-b932-11ea-b3de-0242ac130004",	
                        "attributes":{	
                          "text":"My flower text"	
                        },
                        "relationships":{	
                          "board":{	
                            "id":"b0a10a82-e780-11e9-a359-2a2ae2dbcce4"	
                          }
                        }	
                      }
                    ]
                  }
                }
              ]
            }
          }
        ],
        "meta": {
          "pagination": {
            "total": 2,
            "page": 1,
            "per_page": 20,
            "total_pages": 1
          }
        }
      }
    """

  Scenario: It list meetings successfully filter by name
    When I call "GET" "/api/meetings?filter[attributes.name]=2010"
    Then the status code should be 200
    And the response should matches
    """
      {
        "data":[
          {
            "id":"3d900d32-8d47-11ea-bc55-0242ac130003",
            "attributes":{
              "name":"2010"
            },
            "relationships":{
              "boards":[
                {
                  "id":"0ae12040-4b00-11e9-b475-0800200c9a66",
                  "attributes":{
                    "name":"KEEP",
                    "background_color":null
                  },
                  "relationships":{
                    "meeting":{
                      "id":"3d900d32-8d47-11ea-bc55-0242ac130003"
                    },
                    "cards":[
                      {	
                        "id":"d20dc206-ed97-11e9-81b4-2a2ae2dbcce4",	
                        "attributes":{	
                          "text":"My keep text"	
                        },
                        "relationships":{	
                          "board":{	
                            "id":"0ae12040-4b00-11e9-b475-0800200c9a66"	
                          }
                        }	
                      }
                    ]
                  }
                },
                {
                  "id":"974becbe-e780-11e9-81b4-2a2ae2dbcce4",
                  "attributes":{
                    "name":"START",
                    "background_color":null
                  },
                  "relationships":{
                    "meeting":{
                      "id":"3d900d32-8d47-11ea-bc55-0242ac130003"
                    },
                    "cards":[
                      {	
                        "id":"1c066fdc-b932-11ea-b3de-0242ac130004",	
                        "attributes":{	
                          "text":"My start text"	
                        },
                        "relationships":{	
                          "board":{	
                            "id":"974becbe-e780-11e9-81b4-2a2ae2dbcce4"	
                          }
                        }	
                      }
                    ]
                  }
                }
              ]
            }
          }
        ],
        "meta": {
          "pagination": {
            "total": 1,
            "page": 1,
            "per_page": 20,
            "total_pages": 1
          }
        }
      }
    """

  Scenario: It list meetings successfully order by name DESC
    When I call "GET" "/api/meetings?sortBy[attributes.name]=desc"
    Then the status code should be 200
    And the response should matches
    """
      {
        "data":[
          {
            "id":"09eb1b50-1e9f-11ea-978f-2e728ce88125",
            "attributes":{
              "name":"2014"
            },
            "relationships":{
              "boards":[
                {
                  "id":"a08fd33a-e780-11e9-81b4-2a2ae2dbcce4",
                  "attributes":{
                    "name":"STOP",
                    "background_color":null
                  },
                  "relationships":{
                    "meeting":{
                      "id":"09eb1b50-1e9f-11ea-978f-2e728ce88125"
                    },
                    "cards":[
                      {	
                        "id":"1fdd0bb6-b932-11ea-b3de-0242ac130004",	
                        "attributes":{	
                          "text":"My stop text"	
                        },
                        "relationships":{	
                          "board":{	
                            "id":"a08fd33a-e780-11e9-81b4-2a2ae2dbcce4"	
                          }
                        }	
                      }
                    ]
                  }
                },
                {
                  "id":"b0a10a82-e780-11e9-a359-2a2ae2dbcce4",
                  "attributes":{
                    "name":"FLOWER",
                    "background_color":null
                  },
                  "relationships":{
                    "meeting":{
                      "id":"09eb1b50-1e9f-11ea-978f-2e728ce88125"
                    },
                    "cards":[
                      {	
                        "id":"24806988-b932-11ea-b3de-0242ac130004",	
                        "attributes":{	
                          "text":"My flower text"	
                        },
                        "relationships":{	
                          "board":{	
                            "id":"b0a10a82-e780-11e9-a359-2a2ae2dbcce4"	
                          }
                        }	
                      }
                    ]
                  }
                }
              ]
            }
          },
          {
            "id":"3d900d32-8d47-11ea-bc55-0242ac130003",
            "attributes":{
              "name":"2010"
            },
            "relationships":{
              "boards":[
                {
                  "id":"0ae12040-4b00-11e9-b475-0800200c9a66",
                  "attributes":{
                    "name":"KEEP",
                    "background_color":null
                  },
                  "relationships":{
                    "meeting":{
                      "id":"3d900d32-8d47-11ea-bc55-0242ac130003"
                    },
                    "cards":[
                      {	
                        "id":"d20dc206-ed97-11e9-81b4-2a2ae2dbcce4",	
                        "attributes":{	
                          "text":"My keep text"	
                        },
                        "relationships":{	
                          "board":{	
                            "id":"0ae12040-4b00-11e9-b475-0800200c9a66"	
                          }
                        }	
                      }
                    ]
                  }
                },
                {
                  "id":"974becbe-e780-11e9-81b4-2a2ae2dbcce4",
                  "attributes":{
                    "name":"START",
                    "background_color":null
                  },
                  "relationships":{
                    "meeting":{
                      "id":"3d900d32-8d47-11ea-bc55-0242ac130003"
                    },
                    "cards":[
                      {	
                        "id":"1c066fdc-b932-11ea-b3de-0242ac130004",	
                        "attributes":{	
                          "text":"My start text"	
                        },
                        "relationships":{	
                          "board":{	
                            "id":"974becbe-e780-11e9-81b4-2a2ae2dbcce4"	
                          }
                        }	
                      }
                    ]
                  }
                }
              ]
            }
          }
        ],
        "meta": {
          "pagination": {
            "total": 2,
            "page": 1,
            "per_page": 20,
            "total_pages": 1
          }
        }
      }
    """
