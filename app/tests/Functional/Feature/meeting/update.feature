Feature:
  In order to use RetroBoard
  I want to list meetings

  Background: Creates user and meetings
    Given the following meetings exists:
    | id                                   | name         |
    | 3d900d32-8d47-11ea-bc55-0242ac130003 | OldMeeting |
    | 09eb1b50-1e9f-11ea-978f-2e728ce88125 | NewMeeting |

  Scenario: It updates meeting name
    When I call "PATCH" "/api/meetings/09eb1b50-1e9f-11ea-978f-2e728ce88125" with body:
    """
      {
        "data": {
          "attributes": {
            "name": "NewestMeeting"
          }
        }
      }
    """
    Then the status code should be 204
    And the response should be empty
    Then I call "GET" "/api/meetings/09eb1b50-1e9f-11ea-978f-2e728ce88125"
    And the property "data.attributes.name" should be equals "NewestMeeting"
