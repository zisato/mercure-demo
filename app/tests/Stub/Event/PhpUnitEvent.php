<?php

namespace RetroBoard\Tests\Stub\Event;

use Zisato\EventSourcing\Aggregate\Event\AbstractEvent;

class PhpUnitEvent extends AbstractEvent
{
    const DEFAULT_VERSION = 1;

    protected static function defaultVersion(): int
    {
        return static::DEFAULT_VERSION;
    }
}
