<?php

namespace RetroBoard\Tests\Application\Board\Command\CreateBoard;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use RetroBoard\Application\Board\Command\CreateBoard\CreateBoardCommand;
use RetroBoard\Application\Board\Command\CreateBoard\CreateBoardCommandHandler;
use RetroBoard\Domain\Board\WriteModel\Service\CreateBoardService;
use RetroBoard\Domain\Board\WriteModel\ValueObject\BackgroundColor;
use RetroBoard\Domain\Board\WriteModel\ValueObject\Title;
use Zisato\EventSourcing\Aggregate\Identity\UUID;

class CreateBoardCommandHandlerTest extends TestCase
{
    /** @var CreateBoardService|MockObject $createBoardService */
    private $createBoardService;
    private CreateBoardCommandHandler $commandHandler;

    protected function setUp(): void
    {
        $this->createBoardService = $this->createMock(CreateBoardService::class);
        $this->commandHandler = new CreateBoardCommandHandler($this->createBoardService);
    }

    public function testShouldCallServiceWithArguments(): void
    {
        $id = UUID::generate();
        $title = 'title';
        $meetingId = UUID::generate();
        $backgroundColor = 'red';

        $this->createBoardService->expects($this->once())
            ->method('create')
            ->with(
                $this->equalTo($id),
                $this->equalTo($meetingId),
                $this->equalTo(Title::fromValue($title)),
                $this->equalTo(BackgroundColor::fromValue($backgroundColor))
            );

        $command = new CreateBoardCommand($id->value(), $title, $meetingId->value(), $backgroundColor);

        $this->commandHandler->__invoke($command);
    }
}
