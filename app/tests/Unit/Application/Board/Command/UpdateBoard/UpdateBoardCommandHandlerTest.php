<?php

namespace RetroBoard\Tests\Application\Board\Command\UpdateBoard;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use RetroBoard\Application\Board\Command\UpdateBoard\UpdateBoardCommand;
use RetroBoard\Application\Board\Command\UpdateBoard\UpdateBoardCommandHandler;
use RetroBoard\Domain\Board\WriteModel\Aggregate\Board;
use RetroBoard\Domain\Board\WriteModel\Repository\BoardRepository;
use RetroBoard\Domain\Board\WriteModel\ValueObject\BackgroundColor;
use RetroBoard\Domain\Board\WriteModel\ValueObject\Title;
use Zisato\EventSourcing\Aggregate\Identity\UUID;

class UpdateBoardCommandHandlerTest extends TestCase
{
    /** @var BoardRepository|MockObject $boardRepository */
    private $boardRepository;
    private UpdateBoardCommandHandler $commandHandler;

    protected function setUp(): void
    {
        $this->boardRepository = $this->createMock(BoardRepository::class);
        $this->commandHandler = new UpdateBoardCommandHandler($this->boardRepository);
    }

    public function testShouldChangeTitle(): void
    {
        $id = UUID::generate();
        $title = 'title';

        /** @var Board|MockObject $board */
        $board = $this->createMock(Board::class);
        $board->expects($this->once())
            ->method('changeTitle')
            ->with($this->equalTo(Title::fromValue($title)));

        $this->boardRepository->expects($this->once())
            ->method('get')
            ->with($this->equalTo($id))
            ->willReturn($board);

        $command = new UpdateBoardCommand($id->value(), ['attributes.name'], $title, null);

        $this->commandHandler->__invoke($command);
    }

    public function testShouldChangeBackgroundColor(): void
    {
        $id = UUID::generate();
        $backgroundColor = 'red';

        /** @var Board|MockObject $board */
        $board = $this->createMock(Board::class);
        $board->expects($this->once())
            ->method('changeBackgroundColor')
            ->with($this->equalTo(BackgroundColor::fromValue($backgroundColor)));

        $this->boardRepository->expects($this->once())
            ->method('get')
            ->with($this->equalTo($id))
            ->willReturn($board);

        $command = new UpdateBoardCommand($id->value(), ['attributes.background_color'], null, $backgroundColor);

        $this->commandHandler->__invoke($command);
    }
}
