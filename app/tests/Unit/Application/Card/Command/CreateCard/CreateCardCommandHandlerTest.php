<?php

namespace RetroBoard\Tests\Application\Card\Command\CreateCard;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use RetroBoard\Application\Card\Command\CreateCard\CreateCardCommand;
use RetroBoard\Application\Card\Command\CreateCard\CreateCardCommandHandler;
use RetroBoard\Domain\Card\WriteModel\Service\CreateCardService;
use RetroBoard\Domain\Card\WriteModel\ValueObject\Text;
use Zisato\EventSourcing\Aggregate\Identity\UUID;

class CreateCardCommandHandlerTest extends TestCase
{
    /** @var CreateCardService|MockObject $createCardService */
    private $createCardService;
    private CreateCardCommandHandler $commandHandler;

    protected function setUp(): void
    {
        $this->createCardService = $this->createMock(CreateCardService::class);
        $this->commandHandler = new CreateCardCommandHandler($this->createCardService);
    }

    public function testShouldCallServiceWithArguments(): void
    {
        $id = UUID::generate();
        $boardId = UUID::generate();
        $text = 'text';

        $this->createCardService->expects($this->once())
            ->method('create')
            ->with(
                $this->equalTo($id),
                $this->equalTo($boardId),
                $this->equalTo(Text::fromValue($text))
            );

        $command = new CreateCardCommand($id->value(), $text, $boardId->value());

        $this->commandHandler->__invoke($command);
    }
}
