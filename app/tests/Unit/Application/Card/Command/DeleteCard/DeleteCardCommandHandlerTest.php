<?php

namespace RetroBoard\Tests\Application\Card\Command\DeleteCard;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use RetroBoard\Application\Card\Command\DeleteCard\DeleteCardCommand;
use RetroBoard\Application\Card\Command\DeleteCard\DeleteCardCommandHandler;
use RetroBoard\Domain\Card\WriteModel\Aggregate\Card;
use RetroBoard\Domain\Card\WriteModel\Repository\CardRepository;
use Zisato\EventSourcing\Aggregate\Identity\UUID;

class DeleteCardCommandHandlerTest extends TestCase
{
    /** @var CardRepository|MockObject $cardRepository */
    private $cardRepository;
    private DeleteCardCommandHandler $commandHandler;

    protected function setUp(): void
    {
        $this->cardRepository = $this->createMock(CardRepository::class);
        $this->commandHandler = new DeleteCardCommandHandler($this->cardRepository);
    }

    public function testShouldChangeText(): void
    {
        $id = UUID::generate();

        /** @var Card|MockObject $card */
        $card = $this->createMock(Card::class);
        $card->expects($this->once())
            ->method('delete');

        $this->cardRepository->expects($this->once())
            ->method('get')
            ->with($this->equalTo($id))
            ->willReturn($card);

        $command = new DeleteCardCommand($id->value());

        $this->commandHandler->__invoke($command);
    }
}
