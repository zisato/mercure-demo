<?php

namespace RetroBoard\Tests\Application\Card\Command\UpdateCard;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use RetroBoard\Application\Card\Command\UpdateCard\UpdateCardCommand;
use RetroBoard\Application\Card\Command\UpdateCard\UpdateCardCommandHandler;
use RetroBoard\Domain\Card\WriteModel\Aggregate\Card;
use RetroBoard\Domain\Card\WriteModel\Repository\CardRepository;
use RetroBoard\Domain\Card\WriteModel\ValueObject\Text;
use Zisato\EventSourcing\Aggregate\Identity\UUID;

class UpdateCardCommandHandlerTest extends TestCase
{
    /** @var CardRepository|MockObject $cardRepository */
    private $cardRepository;
    private UpdateCardCommandHandler $commandHandler;

    protected function setUp(): void
    {
        $this->cardRepository = $this->createMock(CardRepository::class);
        $this->commandHandler = new UpdateCardCommandHandler($this->cardRepository);
    }

    public function testShouldChangeText(): void
    {
        $id = UUID::generate();
        $text = 'text';

        /** @var Card|MockObject $card */
        $card = $this->createMock(Card::class);
        $card->expects($this->once())
            ->method('changeText')
            ->with($this->equalTo(Text::fromValue($text)));

        $this->cardRepository->expects($this->once())
            ->method('get')
            ->with($this->equalTo($id))
            ->willReturn($card);

        $command = new UpdateCardCommand($id->value(), ['attributes.text'], $text, null);

        $this->commandHandler->__invoke($command);
    }
}
