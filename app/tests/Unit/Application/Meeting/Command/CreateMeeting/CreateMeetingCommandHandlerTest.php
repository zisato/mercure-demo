<?php

namespace RetroMeeting\Tests\Application\Meeting\Command\CreateMeeting;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use RetroBoard\Application\Meeting\Command\CreateMeeting\CreateMeetingCommand;
use RetroBoard\Application\Meeting\Command\CreateMeeting\CreateMeetingCommandHandler;
use RetroBoard\Domain\Meeting\WriteModel\Service\CreateMeetingService;
use RetroBoard\Domain\Meeting\WriteModel\ValueObject\Name;
use Zisato\EventSourcing\Aggregate\Identity\UUID;

class CreateMeetingCommandHandlerTest extends TestCase
{
    /** @var CreateMeetingService|MockObject $createMeetingService */
    private $createMeetingService;
    private CreateMeetingCommandHandler $commandHandler;

    protected function setUp(): void
    {
        $this->createMeetingService = $this->createMock(CreateMeetingService::class);
        $this->commandHandler = new CreateMeetingCommandHandler($this->createMeetingService);
    }

    public function testShouldCallServiceWithArguments(): void
    {
        $id = UUID::generate();
        $name = 'name';

        $this->createMeetingService->expects($this->once())
            ->method('create')
            ->with(
                $this->equalTo($id),
                $this->equalTo(Name::fromValue($name))
            );

        $command = new CreateMeetingCommand($id->value(), $name);

        $this->commandHandler->__invoke($command);
    }
}
