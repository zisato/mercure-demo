<?php

namespace RetroMeeting\Tests\Application\Meeting\Command\CreateMeeting;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use RetroBoard\Application\Meeting\Command\UpdateMeeting\UpdateMeetingCommand;
use RetroBoard\Application\Meeting\Command\UpdateMeeting\UpdateMeetingCommandHandler;
use RetroBoard\Domain\Meeting\WriteModel\Aggregate\Meeting;
use RetroBoard\Domain\Meeting\WriteModel\Repository\MeetingRepository;
use RetroBoard\Domain\Meeting\WriteModel\ValueObject\Name;
use Zisato\EventSourcing\Aggregate\Identity\UUID;

class UpdateMeetingCommandHandlerTest extends TestCase
{
    /** @var MeetingRepository|MockObject $meetingRepository */
    private $meetingRepository;
    private UpdateMeetingCommandHandler $commandHandler;

    protected function setUp(): void
    {
        $this->meetingRepository = $this->createMock(MeetingRepository::class);
        $this->commandHandler = new UpdateMeetingCommandHandler($this->meetingRepository);
    }

    public function testShouldChangeName(): void
    {
        $id = UUID::generate();
        $name = 'name';

        /** @var Meeting|MockObject $meeting */
        $meeting = $this->createMock(Meeting::class);
        $meeting->expects($this->once())
            ->method('changeName')
            ->with($this->equalTo(Name::fromValue($name)));

        $this->meetingRepository->expects($this->once())
            ->method('get')
            ->with($this->equalTo($id))
            ->willReturn($meeting);

        $command = new UpdateMeetingCommand($id->value(), ['attributes.name'], $name);

        $this->commandHandler->__invoke($command);
    }
}
