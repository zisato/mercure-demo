<?php

declare(strict_types=1);

namespace RetroMeeting\Tests\Application\Meeting\Query\DetailMeeting;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use RetroBoard\Application\Meeting\Query\DetailMeeting\DetailMeetingQuery;
use RetroBoard\Application\Meeting\Query\DetailMeeting\DetailMeetingQueryHandler;
use RetroBoard\Application\Meeting\Transformer\MeetingTransformer;
use RetroBoard\Domain\Meeting\ReadModel\Repository\MeetingsProjectionReadRepository;
use RetroBoard\Domain\Meeting\ReadModel\ValueObject\MeetingProjectionModel;
use RetroBoard\Domain\Meeting\WriteModel\ValueObject\Name;
use Zisato\EventSourcing\Aggregate\Identity\UUID;

class DetailMeetingQueryHandlerTest extends TestCase
{
    /** @var MeetingsProjectionReadRepository|MockObject $meetingsProjectionReadRepository */
    private $meetingsProjectionReadRepository;
    /** @var MeetingTransformer|MockObject $meetingTransformer */
    private $meetingTransformer;
    private DetailMeetingQueryHandler $queryHandler;

    protected function setUp(): void
    {
        $this->meetingsProjectionReadRepository = $this->createMock(MeetingsProjectionReadRepository::class);
        $this->meetingTransformer = $this->createMock(MeetingTransformer::class);
        $this->queryHandler = new DetailMeetingQueryHandler($this->meetingsProjectionReadRepository, $this->meetingTransformer);
    }

    public function testShouldCallServiceWithArguments(): void
    {
        $id = UUID::generate();
        $name = Name::fromValue('Meeting Name');
        $meetingProjection = MeetingProjectionModel::create($id, $name);

        $this->meetingsProjectionReadRepository->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo($id->value()),
            )
            ->willReturn($meetingProjection);

        $this->meetingTransformer->expects($this->once())
            ->method('transform')
            ->with(
                $this->equalTo($meetingProjection),
            );

        $command = new DetailMeetingQuery($id->value());

        $this->queryHandler->__invoke($command);
    }
}
