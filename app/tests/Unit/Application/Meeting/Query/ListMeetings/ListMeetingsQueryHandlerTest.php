<?php

declare(strict_types=1);

namespace RetroMeeting\Tests\Application\Meeting\Query\ListMeetings;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use RetroBoard\Application\Meeting\Query\ListMeetings\ListMeetingsQuery;
use RetroBoard\Application\Meeting\Query\ListMeetings\ListMeetingsQueryHandler;
use RetroBoard\Application\Meeting\Transformer\MeetingTransformer;
use RetroBoard\Domain\Meeting\ReadModel\Repository\MeetingsProjectionReadRepository;
use RetroBoard\Domain\Meeting\ReadModel\ValueObject\MeetingProjectionModel;
use RetroBoard\Domain\Meeting\WriteModel\ValueObject\Name;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\Projection\Criteria\Condition;
use Zisato\Projection\Criteria\Criteria;
use Zisato\Projection\Criteria\CriteriaItem;
use Zisato\Projection\OrderBy\Direction;
use Zisato\Projection\OrderBy\OrderBy;
use Zisato\Projection\OrderBy\OrderByItem;
use Zisato\Projection\ValueObject\ProjectionModelCollection;

class ListMeetingsQueryHandlerTest extends TestCase
{
    /** @var MeetingsProjectionReadRepository|MockObject $meetingsProjectionReadRepository */
    private $meetingsProjectionReadRepository;
    /** @var MeetingTransformer|MockObject $meetingTransformer */
    private $meetingTransformer;
    private ListMeetingsQueryHandler $queryHandler;

    protected function setUp(): void
    {
        $this->meetingsProjectionReadRepository = $this->createMock(MeetingsProjectionReadRepository::class);
        $this->meetingTransformer = $this->createMock(MeetingTransformer::class);
        $this->queryHandler = new ListMeetingsQueryHandler($this->meetingsProjectionReadRepository, $this->meetingTransformer);
    }

    public function testShouldCallServiceWithArguments(): void
    {
        $filters = [
            'attributes.name' => 'Meeting 2',
        ];
        $page = 2;
        $perPage = 42;
        $sortBy = ['attributes.name' => 'asc'];
        $values = [
            MeetingProjectionModel::create(UUID::generate(), Name::fromValue('Meeting 1')),
            MeetingProjectionModel::create(UUID::generate(), Name::fromValue('Meeting 2'))
        ];
        $collection = ProjectionModelCollection::create(2, $values);
        $criteria = new Criteria(new CriteriaItem('attributes.name', 'Meeting 2', Condition::eq()));
        $orderBy = new OrderBy(new OrderByItem('attributes.name', Direction::asc()));

        $this->meetingsProjectionReadRepository->expects($this->once())
            ->method('findBy')
            ->with(
                $this->equalTo($criteria),
                $this->equalTo(($page - 1) * $perPage),
                $this->equalTo($perPage),
                $this->equalTo($orderBy),
            )
            ->willReturn($collection);

        $this->meetingTransformer->expects($this->exactly(2))
            ->method('transform')
            ->withConsecutive(
                [$this->equalTo($values[0])],
                [$this->equalTo($values[1])],
            );

        $command = ListMeetingsQuery::create(
            $filters,
            $page,
            $perPage,
            $sortBy
        );

        $this->queryHandler->__invoke($command);
    }
}
