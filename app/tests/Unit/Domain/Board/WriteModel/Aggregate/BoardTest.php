<?php

namespace RetroBoard\Tests\Domain\Board\WriteModel\Aggregate;

use PHPUnit\Framework\TestCase;
use RetroBoard\Domain\Board\WriteModel\Aggregate\Board;
use RetroBoard\Domain\Board\WriteModel\ValueObject\BackgroundColor;
use RetroBoard\Domain\Board\WriteModel\ValueObject\Title;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Identity\IdentityInterface;

class BoardTest extends TestCase
{
    /**
     * @dataProvider getCreateSuccessfullyData
     */
    public function testCreateSuccessfully(
        IdentityInterface $aggregateId, 
        Title $title, 
        IdentityInterface $meetingId,
        ?BackgroundColor $backgroundColor
    ): void {
        $board = Board::create($aggregateId, $title, $meetingId, $backgroundColor);
        
        $this->assertEquals($board->id(), $aggregateId);
        $this->assertEquals($board->meetingId(), $meetingId);
    }

    /**
     * @dataProvider getChangeMethodsData
     */
    public function testChangeMethods(string $method, int $expectedCount, array $values): void
    {
        $board = Board::create(UUID::generate(), Title::fromValue('Test Title'), UUID::generate(), null);
        $board->releaseRecordedEvents();

        foreach ($values as $value) {
            $board->{$method}($value);
        }

        $events = $board->releaseRecordedEvents();

        $this->assertEquals($expectedCount, $events->count());
    }

    public function getCreateSuccessfullyData(): array
    {
        return [
            [
                UUID::generate(),
                Title::fromValue('Test Title'),
                UUID::generate(),
                null
            ],
            [
                UUID::generate(),
                Title::fromValue('Test Title'),
                UUID::generate(),
                BackgroundColor::fromValue('red')
            ],
        ];
    }

    public function getChangeMethodsData(): array
    {
        return [
            [
                'changeTitle',
                1,
                [
                    Title::fromValue('New Title'),
                    Title::fromValue('New Title'),
                ]
            ],
            [
                'changeTitle',
                2,
                [
                    Title::fromValue('New Title'),
                    Title::fromValue('New Awesome Title'),
                ]
            ],
            [
                'changeBackgroundColor',
                1,
                [
                    BackgroundColor::fromValue('bg-test'),
                ]
            ],
            [
                'changeBackgroundColor',
                1,
                [
                    BackgroundColor::fromValue('bg-test'),
                    BackgroundColor::fromValue('bg-test'),
                ]
            ],
            [
                'changeBackgroundColor',
                0,
                [
                    null,
                ]
            ],
            [
                'changeBackgroundColor',
                3,
                [
                    BackgroundColor::fromValue('bg-test'),
                    null,
                    BackgroundColor::fromValue('bg-test'),
                ]
            ],
        ];
    }
}
