<?php

namespace RetroBoard\Tests\Domain\Board\WriteModel\Event\Upcasters;

use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Aggregate\Event\EventInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use RetroBoard\Domain\Board\WriteModel\Event\BoardCreated;
use RetroBoard\Domain\Board\WriteModel\Event\Upcasters\BoardCreatedV2Upcaster;

class BoardCreatedV2UpcasterTest extends TestCase
{
    /**
     * @dataProvider getCanUpcastData
     */
    public function testCanUpcast(EventInterface $event, bool $expected): void
    {
        $upcaster = new BoardCreatedV2Upcaster();

        $result = $upcaster->canUpcast($event);

        $this->assertEquals($expected, $result);
    }

    public function testItShouldAddBackgroundColor(): void
    {
        $aggreagateId = UUID::generate()->value();
        $aggregateVersion = 0;
        $createdAt = new \DateTimeImmutable();
        $upcaster = new BoardCreatedV2Upcaster();

        /** @var BoardCreated|MockObject $event */
        $event = $this->createMock(BoardCreated::class);
        $event->expects($this->once())
            ->method('payload')
            ->willReturn([]);
        $event->expects($this->once())
            ->method('aggregateId')
            ->willReturn($aggreagateId);
        $event->expects($this->once())
            ->method('aggregateVersion')
            ->willReturn($aggregateVersion);
        $event->expects($this->once())
            ->method('createdAt')
            ->willReturn($createdAt);

        $result = $upcaster->upcast($event);
        $expectedKey = 'background_color';
        $expectedResult = null;

        $this->assertArrayHasKey($expectedKey, $result->payload());
        $this->assertEquals($result->payload()[$expectedKey], $expectedResult);
    }

    public function getCanUpcastData(): array
    {
        /** @var BoardCreated|MockObject $eventToBeHandled */
        $eventToBeHandled = $this->createMock(BoardCreated::class);
        $eventToBeHandled->expects($this->once())
            ->method('version')
            ->willReturn(1);

        /** @var BoardCreated|MockObject $eventNotHandled */
        $eventNotHandled = $this->createMock(BoardCreated::class);
        $eventNotHandled->expects($this->once())
            ->method('version')
            ->willReturn(2);

        return [
            [
                $eventToBeHandled,
                true,
            ],
            [
                $eventNotHandled,
                false,
            ],
        ];
    }
}
