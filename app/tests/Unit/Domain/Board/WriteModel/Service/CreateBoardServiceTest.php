<?php

namespace RetroBoard\Tests\Domain\Board\WriteModel\Service;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use RetroBoard\Domain\Board\WriteModel\Repository\BoardRepository;
use RetroBoard\Domain\Board\WriteModel\Service\CreateBoardService;
use RetroBoard\Domain\Board\WriteModel\ValueObject\Title;
use RetroBoard\Domain\Meeting\WriteModel\Aggregate\Meeting;
use RetroBoard\Domain\Meeting\WriteModel\Repository\MeetingRepository;
use RetroBoard\Domain\Meeting\WriteModel\ValueObject\Name;
use Zisato\EventSourcing\Aggregate\Exception\AggregateRootNotFoundException;
use Zisato\EventSourcing\Aggregate\Exception\DuplicatedAggregateIdException;
use Zisato\EventSourcing\Aggregate\Identity\UUID;

class CreateBoardServiceTest extends TestCase
{
    /** @var MeetingRepository|MockObject $meetingRepository */
    private $meetingRepository;
    /** @var BoardRepository|MockObject $boardRepository */
    private $boardRepository;
    private CreateBoardService $service;

    protected function setUp(): void
    {
        $this->meetingRepository = $this->createMock(MeetingRepository::class);
        $this->boardRepository = $this->createMock(BoardRepository::class);
        $this->service = new CreateBoardService($this->meetingRepository, $this->boardRepository);
    }

    public function testItShouldCreateBoard(): void
    {
        $boardId = UUID::generate();
        $meetingId = UUID::generate();
        $title = Title::fromValue('Board Title');
        $backgroundColor = null;
        $meeting = Meeting::create(UUID::generate(), Name::fromValue('Meeting Name'));

        $this->boardRepository->expects($this->once())
            ->method('get')
            ->with($this->equalTo($boardId))
            ->willThrowException(new AggregateRootNotFoundException());
        $this->boardRepository->expects($this->once())
            ->method('save');

        $this->meetingRepository->expects($this->once())
            ->method('get')
            ->with($this->equalTo($meetingId))
            ->willReturn($meeting);

        $this->service->create($boardId, $meetingId, $title, $backgroundColor);
    }

    public function testItShouldThrowExceptionWhenBoardIdExists(): void
    {
        $this->expectException(DuplicatedAggregateIdException::class);

        $boardId = UUID::generate();
        $meetingId = UUID::generate();
        $title = Title::fromValue('Board Title');
        $backgroundColor = null;

        $this->boardRepository->expects($this->once())
            ->method('get')
            ->with($this->equalTo($boardId));

        $this->service->create($boardId, $meetingId, $title, $backgroundColor);
    }
}
