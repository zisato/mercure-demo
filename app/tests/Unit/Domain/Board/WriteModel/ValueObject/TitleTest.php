<?php

namespace RetroBoard\Tests\Domain\Board\WriteModel\ValueObject;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use RetroBoard\Domain\Board\WriteModel\ValueObject\Title;

class TitleTest extends TestCase
{
    /**
     * @dataProvider getInvalidData
     */
    public function testItShouldThrowInvalidArgumentExceptionWhenInvalidValue(string $value)
    {
        $this->expectException(InvalidArgumentException::class);

        Title::fromValue($value);
    }

    public function getInvalidData(): array
    {
        return [
            [''],
            [
                '0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789'
                . '0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789'
                . '01234567890123456789012345678901234567890123456789012345'
            ]
        ];
    }
}
