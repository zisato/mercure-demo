<?php

namespace RetroBoard\Tests\Domain\Card\WriteModel\Aggregate;

use PHPUnit\Framework\TestCase;
use RetroBoard\Domain\Card\WriteModel\Aggregate\Card;
use RetroBoard\Domain\Card\WriteModel\ValueObject\Text;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Identity\IdentityInterface;

class CardTest extends TestCase
{
    /**
     * @dataProvider getCreateSuccessfullyData
     */
    public function testCreateSuccessfully(
        IdentityInterface $aggregateId, 
        Text $text, 
        IdentityInterface $boarId
    ): void {
        $card = Card::create($aggregateId, $text, $boarId);
        
        $this->assertEquals($card->id(), $aggregateId);
        $this->assertEquals($card->text(), $text);
        $this->assertEquals($card->boardId(), $boarId);
    }

    /**
     * @dataProvider getChangeMethodsData
     */
    public function testChangeMethods(string $method, int $expectedCount, array $values): void
    {
        $card = Card::create(UUID::generate(), Text::fromValue('Test Text'), UUID::generate(), UUID::generate());
        $card->releaseRecordedEvents();

        foreach ($values as $value) {
            $card->{$method}($value);
        }

        $events = $card->releaseRecordedEvents();

        $this->assertEquals($expectedCount, $events->count());
    }

    public function testDeleteWhenIsDeletedPreviously(): void
    {
        $card = Card::create(UUID::generate(), Text::fromValue('Test Text'), UUID::generate(), UUID::generate());
        $card->delete();
        $expectedCount = 0;
        $card->releaseRecordedEvents();

        $card->delete();

        $events = $card->releaseRecordedEvents();

        $this->assertEquals($expectedCount, $events->count());
    }

    public function getCreateSuccessfullyData(): array
    {
        return [
            [
                UUID::generate(),
                Text::fromValue('Test Title'),
                UUID::generate(),
                UUID::generate()
            ],
        ];
    }

    public function getChangeMethodsData(): array
    {
        return [
            [
                'changeText',
                1,
                [
                    Text::fromValue('New Text'),
                    Text::fromValue('New Text'),
                ]
            ],
            [
                'changeText',
                2,
                [
                    Text::fromValue('New Text'),
                    Text::fromValue('New Awesome Text'),
                ]
            ],
        ];
    }
}
