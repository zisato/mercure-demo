<?php

namespace RetroBoard\Tests\Domain\Card\WriteModel\Service;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use RetroBoard\Domain\Board\WriteModel\Aggregate\Board;
use RetroBoard\Domain\Board\WriteModel\Repository\BoardRepository;
use RetroBoard\Domain\Board\WriteModel\ValueObject\Title;
use RetroBoard\Domain\Card\WriteModel\Repository\CardRepository;
use RetroBoard\Domain\Card\WriteModel\Service\CreateCardService;
use RetroBoard\Domain\Card\WriteModel\ValueObject\Text;
use Zisato\EventSourcing\Aggregate\Exception\AggregateRootNotFoundException;
use Zisato\EventSourcing\Aggregate\Exception\DuplicatedAggregateIdException;
use Zisato\EventSourcing\Aggregate\Identity\UUID;

class CreateCardServiceTest extends TestCase
{
    /** @var CardRepository|MockObject $cardRepository */
    private $cardRepository;
    /** @var BoardRepository|MockObject $boardRepository */
    private $boardRepository;
    private CreateCardService $service;

    protected function setUp(): void
    {
        $this->cardRepository = $this->createMock(CardRepository::class);
        $this->boardRepository = $this->createMock(BoardRepository::class);
        $this->service = new CreateCardService($this->cardRepository, $this->boardRepository);
    }

    public function testItShouldCreateBoard(): void
    {
        $cardId = UUID::generate();
        $boardId = UUID::generate();
        $text = Text::fromValue('Card Text');
        $board = Board::create(UUID::generate(), Title::fromValue('Board Title'), UUID::generate(), null);

        $this->cardRepository->expects($this->once())
            ->method('get')
            ->with($this->equalTo($cardId))
            ->willThrowException(new AggregateRootNotFoundException());
        $this->cardRepository->expects($this->once())
            ->method('save');

        $this->boardRepository->expects($this->once())
            ->method('get')
            ->with($this->equalTo($boardId))
            ->willReturn($board);

        $this->service->create($cardId, $boardId, $text);
    }

    public function testItShouldThrowExceptionWhenCardIdExists(): void
    {
        $this->expectException(DuplicatedAggregateIdException::class);

        $cardId = UUID::generate();
        $boardId = UUID::generate();
        $text = Text::fromValue('Card Text');

        $this->cardRepository->expects($this->once())
            ->method('get')
            ->with($this->equalTo($cardId));

        $this->service->create($cardId, $boardId, $text);
    }
}
