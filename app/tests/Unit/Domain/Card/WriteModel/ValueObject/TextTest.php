<?php

namespace RetroBoard\Tests\Domain\Card\WriteModel\ValueObject;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use RetroBoard\Domain\Card\WriteModel\ValueObject\Text;

class TextTest extends TestCase
{
    /**
     * @dataProvider getInvalidData
     */
    public function testItShouldThrowInvalidArgumentExceptionWhenInvalidValue(string $value)
    {
        $this->expectException(InvalidArgumentException::class);

        Text::fromValue($value);
    }

    public function getInvalidData(): array
    {
        return [
            [''],
            [
                "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789"
                . "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789"
                . "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789"
                . "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789"
                . "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789"
                . "0"
            ]
        ];
    }
}
