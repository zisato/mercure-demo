<?php

namespace RetroBoard\Tests\Domain\Meeting\ReadModel\ValueObject;

use PHPUnit\Framework\TestCase;
use RetroBoard\Domain\Board\WriteModel\ValueObject\BackgroundColor;
use RetroBoard\Domain\Board\WriteModel\ValueObject\Title;
use RetroBoard\Domain\Card\WriteModel\ValueObject\Text;
use RetroBoard\Domain\Meeting\ReadModel\ValueObject\BoardProjectionModel;
use RetroBoard\Domain\Meeting\ReadModel\ValueObject\CardProjectionModel;
use Zisato\EventSourcing\Aggregate\Identity\UUID;

class BoardProjectionModelTest extends TestCase
{
    public function testShouldCreate(): void
    {
        $id = UUID::generate();
        $title = Title::fromValue('title');
        $backgroundColor = BackgroundColor::fromValue('red');
        $meetingId = UUID::generate();

        $projection = BoardProjectionModel::create($id, $title, $backgroundColor, $meetingId);

        $this->assertEquals($id->value(), $projection->data()['id']);
        $this->assertEquals($title->value(), $projection->data()['attributes']['name']);
        $this->assertEquals($backgroundColor->value(), $projection->data()['attributes']['background_color']);
        $this->assertEquals($meetingId->value(), $projection->data()['relationships']['meeting']['id']);
    }

    public function testShouldChangeTitle(): void
    {
        $newTitle = Title::fromValue('title');

        $projection = BoardProjectionModel::create(UUID::generate(), Title::fromValue('title'), null, UUID::generate());

        $projection->changeTitle($newTitle);

        $this->assertEquals($newTitle->value(), $projection->data()['attributes']['name']);
    }

    public function testShouldChangeBackgroundColor(): void
    {
        $newBackgroundColor = BackgroundColor::fromValue('red');

        $projection = BoardProjectionModel::create(UUID::generate(), Title::fromValue('title'), null, UUID::generate());

        $projection->changeBackgroundColor($newBackgroundColor);

        $this->assertEquals($newBackgroundColor->value(), $projection->data()['attributes']['background_color']);
    }

    public function testShouldAddCard(): void
    {
        $cardProjection = CardProjectionModel::create(UUID::generate(), Text::fromValue('text'), UUID::generate());

        $projection = BoardProjectionModel::create(UUID::generate(), Title::fromValue('title'), null, UUID::generate());

        $projection->addCard($cardProjection);

        $this->assertCount(1, $projection->data()['relationships']['cards']);
    }

    public function testShouldRemoveCard(): void
    {
        $cardId = UUID::generate();

        $projection = BoardProjectionModel::create(UUID::generate(), Title::fromValue('title'), null, UUID::generate());
        $projection->addCard(CardProjectionModel::create($cardId, Text::fromValue('text'), UUID::generate()));

        $projection->removeCard($cardId);

        $this->assertCount(0, $projection->data()['relationships']['cards']);
    }

    public function testShouldChangeCardText(): void
    {
        $cardId = UUID::generate();
        $newText = Text::fromValue('New Text');

        $projection = BoardProjectionModel::create(UUID::generate(), Title::fromValue('title'), null, UUID::generate());
        $projection->addCard(CardProjectionModel::create($cardId, Text::fromValue('text'), UUID::generate()));

        $projection->changeCardText($cardId, $newText);

        $this->assertEquals($newText->value(), $projection->data()['relationships']['cards'][0]['attributes']['text']);
    }
}
