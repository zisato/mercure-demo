<?php

namespace RetroBoard\Tests\Domain\Meeting\ReadModel\ValueObject;

use PHPUnit\Framework\TestCase;
use RetroBoard\Domain\Card\WriteModel\ValueObject\Text;
use RetroBoard\Domain\Meeting\ReadModel\ValueObject\CardProjectionModel;
use Zisato\EventSourcing\Aggregate\Identity\UUID;

class CardProjectionModelTest extends TestCase
{
    public function testShouldCreate(): void
    {
        $id = UUID::generate();
        $text = Text::fromValue('text');
        $boardId = UUID::generate();

        $projection = CardProjectionModel::create($id, $text, $boardId);

        $this->assertEquals($id->value(), $projection->data()['id']);
        $this->assertEquals($text->value(), $projection->data()['attributes']['text']);
        $this->assertEquals($boardId->value(), $projection->data()['relationships']['board']['id']);
    }

    public function testShouldChangeText(): void
    {
        $newText = Text::fromValue('text');

        $projection = CardProjectionModel::create(UUID::generate(), Text::fromValue('text'), UUID::generate());

        $projection->changeText($newText);

        $this->assertEquals($newText->value(), $projection->data()['attributes']['text']);
    }
}
