<?php

namespace RetroBoard\Tests\Domain\Meeting\ReadModel\ValueObject;

use PHPUnit\Framework\TestCase;
use RetroBoard\Domain\Board\WriteModel\ValueObject\BackgroundColor;
use RetroBoard\Domain\Board\WriteModel\ValueObject\Title;
use RetroBoard\Domain\Card\WriteModel\ValueObject\Text;
use RetroBoard\Domain\Meeting\ReadModel\ValueObject\BoardProjectionModel;
use RetroBoard\Domain\Meeting\ReadModel\ValueObject\CardProjectionModel;
use RetroBoard\Domain\Meeting\ReadModel\ValueObject\MeetingProjectionModel;
use RetroBoard\Domain\Meeting\WriteModel\ValueObject\Name;
use Zisato\EventSourcing\Aggregate\Identity\UUID;

class MeetingProjectionModelTest extends TestCase
{
    public function testShouldCreate(): void
    {
        $id = UUID::generate();
        $name = Name::fromValue('name');

        $projection = MeetingProjectionModel::create($id, $name);

        $this->assertEquals($id->value(), $projection->data()['id']);
        $this->assertEquals($name->value(), $projection->data()['attributes']['name']);
    }

    public function testShouldChangeName(): void
    {
        $newName = Name::fromValue('New Name');

        $projection = MeetingProjectionModel::create(UUID::generate(), Name::fromValue('name'));

        $projection->changeName($newName);

        $this->assertEquals($newName->value(), $projection->data()['attributes']['name']);
    }

    public function testShouldAddBoard(): void
    {
        $boardProjection = BoardProjectionModel::create(UUID::generate(), Title::fromValue('title'), null, UUID::generate());

        $projection = MeetingProjectionModel::create(UUID::generate(), Name::fromValue('name'));

        $projection->addBoard($boardProjection);

        $this->assertCount(1, $projection->data()['relationships']['boards']);
    }

    public function testShouldChangeBoardName(): void
    {
        $boardId = UUID::generate();
        $newTitle = Title::fromValue('New Title');

        $projection = MeetingProjectionModel::create(UUID::generate(), Name::fromValue('name'));

        $projection->changeBoardTitle($boardId, $newTitle);

        $this->assertEquals($newTitle->value(), $projection->data()['relationships']['boards'][0]['attributes']['name']);
    }
}
