<?php

namespace RetroBoard\Tests\Domain\Meeting\WriteModel\Aggregate;

use PHPUnit\Framework\TestCase;
use RetroBoard\Domain\Meeting\WriteModel\Aggregate\Meeting;
use RetroBoard\Domain\Meeting\WriteModel\ValueObject\Name;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Identity\IdentityInterface;

class MeetingTest extends TestCase
{
    /**
     * @dataProvider getCreateSuccessfullyData
     */
    public function testCreateSuccessfully(
        IdentityInterface $aggregateId, 
        Name $name
    ): void {
        $meeting = Meeting::create($aggregateId, $name);
        
        $this->assertEquals($meeting->id(), $aggregateId);
        $this->assertEquals($meeting->name(), $name);
    }

    /**
     * @dataProvider getChangeMethodsData
     */
    public function testChangeMethods(string $method, int $expectedCount, array $values): void
    {
        $meeting = Meeting::create(UUID::generate(), Name::fromValue('Meeting Name'));
        $meeting->releaseRecordedEvents();

        foreach ($values as $value) {
            $meeting->{$method}($value);
        }

        $events = $meeting->releaseRecordedEvents();

        $this->assertEquals($expectedCount, $events->count());
    }

    public function getCreateSuccessfullyData(): array
    {
        return [
            [
                UUID::generate(),
                Name::fromValue('Test Name'),
            ],
        ];
    }

    public function getChangeMethodsData(): array
    {
        return [
            [
                'changeName',
                1,
                [
                    Name::fromValue('New Name'),
                    Name::fromValue('New Name'),
                ]
            ],
            [
                'changeName',
                2,
                [
                    Name::fromValue('New Name'),
                    Name::fromValue('New Awesome Name'),
                ]
            ],
        ];
    }
}
