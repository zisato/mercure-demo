<?php

namespace RetroBoard\Tests\Domain\Meeting\WriteModel\Service;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use RetroBoard\Domain\Meeting\WriteModel\Repository\MeetingRepository;
use RetroBoard\Domain\Meeting\WriteModel\Service\CreateMeetingService;
use RetroBoard\Domain\Meeting\WriteModel\ValueObject\Name;
use Zisato\EventSourcing\Aggregate\Exception\AggregateRootNotFoundException;
use Zisato\EventSourcing\Aggregate\Exception\DuplicatedAggregateIdException;
use Zisato\EventSourcing\Aggregate\Identity\UUID;

class CreateMeetingServiceTest extends TestCase
{
    /** @var MeetingRepository|MockObject $meetingRepository */
    private $meetingRepository;
    private CreateMeetingService $service;

    protected function setUp(): void
    {
        $this->meetingRepository = $this->createMock(MeetingRepository::class);
        $this->service = new CreateMeetingService($this->meetingRepository);
    }

    public function testItShouldCreateBoard(): void
    {
        $meetingId = UUID::generate();
        $name = Name::fromValue('Meeting Name');

        $this->meetingRepository->expects($this->once())
            ->method('get')
            ->with($this->equalTo($meetingId))
            ->willThrowException(new AggregateRootNotFoundException());
        $this->meetingRepository->expects($this->once())
            ->method('save');

        $this->service->create($meetingId, $name);
    }

    public function testItShouldThrowExceptionWhenMeetingIdExists(): void
    {
        $this->expectException(DuplicatedAggregateIdException::class);

        $meetingId = UUID::generate();
        $name = Name::fromValue('Meeting Name');

        $this->meetingRepository->expects($this->once())
            ->method('get')
            ->with($this->equalTo($meetingId));

        $this->service->create($meetingId, $name);
    }
}
