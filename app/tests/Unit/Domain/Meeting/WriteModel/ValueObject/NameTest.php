<?php

namespace RetroBoard\Tests\Domain\Meeting\WriteModel\ValueObject;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use RetroBoard\Domain\Meeting\WriteModel\ValueObject\Name;

class NameTest extends TestCase
{
    /**
     * @dataProvider getInvalidData
     */
    public function testItShouldThrowInvalidArgumentExceptionWhenInvalidValue(string $value)
    {
        $this->expectException(InvalidArgumentException::class);

        Name::fromValue($value);
    }

    public function getInvalidData(): array
    {
        return [
            [''],
            [
                "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789"
                . "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789"
                . "01234567890123456789012345678901234567890123456789"
                . "012345"
            ]
        ];
    }
}
