#!/usr/bin/env bash

container="$1"
shift

case "$container" in
  mercure)
    container=$(docker-compose -f docker/docker-compose.yml ps -q mercure)
    docker exec -it ${container} bash
    ;;
  mysql)
    container=$(docker-compose -f docker/docker-compose.yml ps -q mysql)
    docker exec -it ${container} bash
    ;;
  mongodb)
    container=$(docker-compose -f docker/docker-compose.yml ps -q mongodb)
    docker exec -it ${container} bash
    ;;
  php)
    container=$(docker-compose -f docker/docker-compose.yml ps -q php)
    docker exec -it ${container} bash
    ;;
  *)
    echo "First argument allowed values are: mercure, mysql, mongodb, php"
    ;;
esac