#!/usr/bin/env bash

docker-compose -f docker/docker-compose.yml pull
docker-compose -f docker/docker-compose.yml run php composer $*
exitCode=$?
docker-compose -f docker/docker-compose.yml down
exit $exitCode