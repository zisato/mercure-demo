#!/usr/bin/env bash

docker-compose -f docker/docker-compose.yml pull
docker-compose -f docker/docker-compose.yml run php bin/console $*
exitCode=$?
docker-compose -f docker/docker-compose.yml down
exit $exitCode