#!/usr/bin/env bash

docker-compose -f docker/docker-compose.yml pull
docker-compose -f docker/docker-compose.yml run php bin/phpstan analyse $*
exitCode=$?
docker-compose -f docker/docker-compose.yml down
exit $exitCode