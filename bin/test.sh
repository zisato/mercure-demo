#!/usr/bin/env bash

docker-compose -f app/tests/docker/docker-compose.yml pull
docker-compose -f app/tests/docker/docker-compose.yml run php tests/run.sh $*
exitCode=$?
docker-compose -f app/tests/docker/docker-compose.yml down
exit $exitCode