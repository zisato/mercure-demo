#!/usr/bin/env bash

container="$1"
shift

case "$container" in
  all)
    docker build -t registry.gitlab.com/zisato/retroboard/mercure:latest -f ./mercure/Dockerfile .
    docker push registry.gitlab.com/zisato/retroboard/mercure:latest

    docker build -t registry.gitlab.com/zisato/retroboard/mysql:latest -f ./mysql/Dockerfile .
    docker push registry.gitlab.com/zisato/retroboard/mysql:latest

    docker build -t registry.gitlab.com/zisato/retroboard/mongodb:latest -f ./mongodb/Dockerfile .
    docker push registry.gitlab.com/zisato/retroboard/mongodb:latest

    docker build -t registry.gitlab.com/zisato/retroboard/php:latest -f ./php/php-7.4/Dockerfile .
    docker push registry.gitlab.com/zisato/retroboard/php:latest
    
    docker build -t registry.gitlab.com/zisato/retroboard/php-pcov:latest -f ./php/php-7.4-pcov/Dockerfile .
    docker push registry.gitlab.com/zisato/retroboard/php-pcov:latest

    docker build -t registry.gitlab.com/zisato/retroboard/php-xdebug:latest -f ./php/php-7.4-xdebug/Dockerfile .
    docker push registry.gitlab.com/zisato/retroboard/php-xdebug:latest
    ;;
  mercure)
    docker build -t registry.gitlab.com/zisato/retroboard/mercure:latest -f ./mercure/Dockerfile .
    docker push registry.gitlab.com/zisato/retroboard/mercure:latest
    ;;
  mysql)
    docker build -t registry.gitlab.com/zisato/retroboard/mysql:latest -f ./mysql/Dockerfile .
    docker push registry.gitlab.com/zisato/retroboard/mysql:latest
    ;;
  mongodb)
    docker build -t registry.gitlab.com/zisato/retroboard/mongodb:latest -f ./mongodb/Dockerfile .
    docker push registry.gitlab.com/zisato/retroboard/mongodb:latest
    ;;
  php)
    docker build -t registry.gitlab.com/zisato/retroboard/php:latest -f ./php/php-7.4/Dockerfile .
    docker push registry.gitlab.com/zisato/retroboard/php:latest
    ;;
  php-pcov)
    docker build -t registry.gitlab.com/zisato/retroboard/php-pcov:latest -f ./php/php-7.4-pcov/Dockerfile .
    docker push registry.gitlab.com/zisato/retroboard/php-pcov:latest
    ;;
  php-xdebug)
    docker build -t registry.gitlab.com/zisato/retroboard/php-xdebug:latest -f ./php/php-7.4-xdebug/Dockerfile .
    docker push registry.gitlab.com/zisato/retroboard/php-xdebug:latest
    ;;
  *)
    echo "First argument allowed values are: all, mercure, mysql, mongodb, php, php-pcov, php-xdebug"
    ;;
esac